## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config-endringer, logisk gruppert og lett tilgjengelig

# Main content
firedragon-header = { -brand-short-name } Innstillinger


firedragon-general-heading = Nettleserens oppførsel

firedragon-extension-update-checkbox =
    .label = Oppdater tilleggsprogrammer automatisk
firedragon-extension-update-description = Holder utvidelser oppdatert uten manuell inngripen. Et godt valg for sikkerheten din. Hvis du ikke går gjennom koden til utvidelsene dine før hver oppdatering, bør du aktivere dette alternativet.

firedragon-sync-checkbox =
    .label = Aktiver Firefox Sync
firedragon-sync-description = Synkroniser dataene dine med andre nettlesere. Firefox Sync krypterer data lokalt før de overføres til serveren. Krever omstart.

firedragon-autocopy-checkbox =
    .label = Aktiver lim inn med midtklikk
firedragon-autocopy-description = Marker tekst for å kopiere den, og lim den deretter inn ved å klikke med musen i midten.

firedragon-styling-checkbox =
    .label = Tillat tilpasning av userChrome.css
firedragon-styling-description = Aktiver dette hvis du vil tilpasse brukergrensesnittet med et manuelt innlastet tema. Sørg for at du stoler på leverandøren av temaet.


firedragon-network-heading = Nettverksbygging

firedragon-ipv6-checkbox =
    .label = Aktiver IPv6
firedragon-ipv6-description = Tillat { -brand-short-name } å koble til ved hjelp av IPv6. I stedet for å blokkere IPv6 i nettleseren, foreslår vi at du aktiverer IPv6-personvernsutvidelsen i operativsystemet ditt.


firedragon-privacy-heading = Personvern

firedragon-xorigin-ref-checkbox =
    .label = Begrens henvisninger på tvers av opprinnelse
firedragon-xorigin-ref-description = Send en henvisning kun på samme opprinnelse. Dette forårsaker brudd. I tillegg vil henvisninger fortsatt bli trimmet, selv når de er sendt.


firedragon-broken-heading = Fingeravtrykk

firedragon-rfp-checkbox =
    .label = Aktivere ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting er det beste anti-fingeravtrykksverktøyet i klassen. Hvis du trenger å deaktivere det, bør du vurdere å bruke en utvidelse som Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Aktiver letterboxing
firedragon-letterboxing-description = Letterboxing bruker marginer rundt vinduene dine for å returnere et begrenset sett med avrundede oppløsninger.

firedragon-auto-decline-canvas-checkbox =
    .label = Blokker stille forespørsler om tilgang til lerret
firedragon-auto-decline-canvas-description = Nekter automatisk canvas-tilgang til nettsteder, uten å spørre brukeren. Det er fortsatt mulig å tillate tilgang til lerretet fra url-linjen.

firedragon-webgl-checkbox =
    .label = Aktiver WebGL
firedragon-webgl-description = WebGL er en sterk fingeravtrykksvektor. Hvis du trenger å aktivere det, bør du vurdere å bruke en utvidelse som Canvas Blocker.


firedragon-security-heading = Sikkerhet

firedragon-ocsp-checkbox =
    .label = Håndhev OCSP hard-fail
firedragon-ocsp-description = Forhindre tilkobling til et nettsted hvis OCSP-sjekken ikke kan utføres. Dette øker sikkerheten, men det vil føre til brudd når en OCSP-server er nede.

firedragon-goog-safe-checkbox =
    .label = Aktiver Google Safe Browsing
firedragon-goog-safe-description = Hvis du er bekymret for skadelig programvare og phishing, bør du vurdere å aktivere det. Deaktivert av hensyn til sensur, men anbefales for mindre avanserte brukere. Alle kontrollene skjer lokalt.

firedragon-goog-safe-download-checkbox =
    .label = Skann nedlastinger
firedragon-goog-safe-download-description = La Safe Browsing skanne nedlastningene dine for å identifisere mistenkelige filer. Alle kontrollene skjer lokalt.

# Footer
firedragon-footer = Nyttige lenker
firedragon-config-link =
    .label = Alle avanserte innstillinger (about:config)
firedragon-open-profile =
    .label = Åpne brukerprofilkatalogen
