## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = apie:config pakeitimai, logiškai sugrupuoti ir lengvai pasiekiami

# Main content
firedragon-header = { -brand-short-name } Nustatymai


firedragon-general-heading = Naršyklės elgesys

firedragon-extension-update-checkbox =
    .label = Automatinis priedų atnaujinimas
firedragon-extension-update-description = Atnaujinkite plėtinius be rankinio įsikišimo. Geras pasirinkimas jūsų saugumui užtikrinti. Jei prieš kiekvieną atnaujinimą neperžiūrite savo plėtinių kodo, turėtumėte įjungti šią parinktį.

firedragon-sync-checkbox =
    .label = Įjungti "Firefox" sinchronizavimą
firedragon-sync-description = Sinchronizuokite duomenis su kitomis naršyklėmis. Prieš perduodama duomenis į serverį, "Firefox Sync" juos užšifruoja vietoje. Reikia paleisti iš naujo.

firedragon-autocopy-checkbox =
    .label = Įjungti vidurinio paspaudimo įklijavimą
firedragon-autocopy-description = Pasirinkite tekstą, kad jį nukopijuotumėte, tada jį įklijuokite spustelėję vidurinį pelės klavišą.

firedragon-styling-checkbox =
    .label = Leisti pritaikyti userChrome.css
firedragon-styling-description = Įjunkite šią parinktį, jei norite pritaikyti vartotojo sąsają naudodami rankiniu būdu įkeltą temą. Įsitikinkite, kad pasitikite temos tiekėju.


firedragon-network-heading = Tinklo kūrimas

firedragon-ipv6-checkbox =
    .label = Įjungti IPv6
firedragon-ipv6-description = Leisti { -brand-short-name } prisijungti naudojant IPv6. Užuot naršyklėje blokavę IPv6, siūlome operacinėje sistemoje įjungti IPv6 privatumo plėtinį.


firedragon-privacy-heading = Privatumas

firedragon-xorigin-ref-checkbox =
    .label = Apriboti kryžminės kilmės nuorodas
firedragon-xorigin-ref-description = Siųskite nuorodą tik tos pačios kilmės. Dėl to atsiranda trikdžių. Be to, net ir išsiuntus nuorodas, jos vis tiek bus apkarpytos.


firedragon-broken-heading = Pirštų atspaudų ėmimas

firedragon-rfp-checkbox =
    .label = Įjungti funkciją ResistFingerprinting
firedragon-rfp-description = "ResistFingerprinting" yra geriausias savo klasėje įrankis nuo pirštų atspaudų. Jei norite ją išjungti, apsvarstykite galimybę naudoti tokį plėtinį kaip "Canvas Blocker".

firedragon-letterboxing-checkbox =
    .label = Įjungti raidžių langelį
firedragon-letterboxing-description = Norint grąžinti ribotą suapvalintų rezoliucijų rinkinį, langai apjuosiami paraštėmis.

firedragon-auto-decline-canvas-checkbox =
    .label = Tyliai blokuokite drobės prieigos užklausas
firedragon-auto-decline-canvas-description = Automatiškai, neprašant naudotojo, neleiskite prieiti prie interneto svetainių. Vis dar galima leisti "Canvas" prieigą iš url juostos.

firedragon-webgl-checkbox =
    .label = Įjungti "WebGL"
firedragon-webgl-description = "WebGL" yra stiprus pirštų atspaudų vektorius. Jei reikia jį įjungti, apsvarstykite galimybę naudoti tokį plėtinį kaip "Canvas Blocker".


firedragon-security-heading = Apsauga

firedragon-ocsp-checkbox =
    .label = Įgyvendinti "OCSP hard-fail"
firedragon-ocsp-description = Neleiskite prisijungti prie svetainės, jei OCSP patikra negali būti atlikta. Taip padidinamas saugumas, tačiau tai sukels trikdžių, kai OCSP serveris neveikia.

firedragon-goog-safe-checkbox =
    .label = Įgalinkite "Google" saugų naršymą
firedragon-goog-safe-description = Jei nerimaujate dėl kenkėjiškų programų ir sukčiavimo, apsvarstykite galimybę įjungti šią funkciją. Išjungta dėl susirūpinimo cenzūra, bet rekomenduojama mažiau pažengusiems naudotojams. Visi patikrinimai atliekami vietoje.

firedragon-goog-safe-download-checkbox =
    .label = Skenuoti atsisiunčiamus dokumentus
firedragon-goog-safe-download-description = Leiskite "Saugiam naršymui" nuskaityti atsisiunčiamus failus ir nustatyti įtartinus failus. Visi patikrinimai atliekami vietoje.

# Footer
firedragon-footer = Naudingos nuorodos
firedragon-config-link =
    .label = Visi išplėstiniai nustatymai (about:config)
firedragon-open-profile =
    .label = Atidarykite naudotojo profilio katalogą
