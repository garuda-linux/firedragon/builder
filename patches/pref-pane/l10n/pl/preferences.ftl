## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = zmiany about:config, logicznie pogrupowane i łatwo dostępne

# Main content
firedragon-header = { -brand-short-name } Preferencje


firedragon-general-heading = Zachowanie przeglądarki

firedragon-extension-update-checkbox =
    .label = Automatyczna aktualizacja dodatków
firedragon-extension-update-description = Aktualizuj rozszerzenia bez ręcznej interwencji. Dobry wybór dla bezpieczeństwa. Jeśli nie sprawdzasz kodu swoich rozszerzeń przed każdą aktualizacją, powinieneś włączyć tę opcję.

firedragon-sync-checkbox =
    .label = Włącz synchronizację Firefoksa
firedragon-sync-description = Synchronizacja danych z innymi przeglądarkami. Firefox Sync szyfruje dane lokalnie przed przesłaniem ich na serwer. Wymaga ponownego uruchomienia.

firedragon-autocopy-checkbox =
    .label = Włącz wklejanie środkowym kliknięciem
firedragon-autocopy-description = Zaznacz tekst, aby go skopiować, a następnie wklej go, klikając środkowym przyciskiem myszy.

firedragon-styling-checkbox =
    .label = Zezwól na dostosowanie userChrome.css
firedragon-styling-description = Włącz tę opcję, jeśli chcesz dostosować interfejs użytkownika za pomocą ręcznie załadowanego motywu. Upewnij się, że ufasz dostawcy motywu.


firedragon-network-heading = Tworzenie sieci

firedragon-ipv6-checkbox =
    .label = Włącz IPv6
firedragon-ipv6-description = Allow { -brand-short-name } to connect using IPv6. Zamiast blokować IPv6 w przeglądarce, sugerujemy włączenie rozszerzenia prywatności IPv6 w systemie operacyjnym.


firedragon-privacy-heading = Prywatność

firedragon-xorigin-ref-checkbox =
    .label = Ograniczenie odsyłaczy pochodzących z różnych źródeł
firedragon-xorigin-ref-description = Wysyłaj referrer tylko w przypadku tego samego pochodzenia. Powoduje to uszkodzenie. Dodatkowo, nawet po wysłaniu odsyłacze nadal będą przycinane.


firedragon-broken-heading = Pobieranie odcisków palców

firedragon-rfp-checkbox =
    .label = Włącz ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting to najlepsze w swojej klasie narzędzie zapobiegające odciskom palców. Jeśli chcesz go wyłączyć, rozważ użycie rozszerzenia takiego jak Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Włącz letterboxing
firedragon-letterboxing-description = Letterbox stosuje marginesy wokół okien, aby zwrócić ograniczony zestaw zaokrąglonych rozdzielczości.

firedragon-auto-decline-canvas-checkbox =
    .label = Ciche blokowanie żądań dostępu do kanwy
firedragon-auto-decline-canvas-description = Automatycznie odmawiaj dostępu canvas do stron internetowych, bez monitowania użytkownika. Nadal możliwe jest zezwolenie na dostęp do kanwy z paska urlbar.

firedragon-webgl-checkbox =
    .label = Włącz WebGL
firedragon-webgl-description = WebGL jest silnym wektorem fingerprintingu. Jeśli musisz go włączyć, rozważ użycie rozszerzenia takiego jak Canvas Blocker.


firedragon-security-heading = Bezpieczeństwo

firedragon-ocsp-checkbox =
    .label = Wymuszenie twardego błędu OCSP
firedragon-ocsp-description = Zapobiega łączeniu się z witryną, jeśli nie można wykonać sprawdzenia OCSP. Zwiększa to bezpieczeństwo, ale powoduje awarię, gdy serwer OCSP nie działa.

firedragon-goog-safe-checkbox =
    .label = Włącz Bezpieczne przeglądanie Google
firedragon-goog-safe-description = Jeśli obawiasz się złośliwego oprogramowania i phishingu, rozważ włączenie tej funkcji. Wyłączona z powodu obaw o cenzurę, ale zalecana dla mniej zaawansowanych użytkowników. Wszystkie kontrole odbywają się lokalnie.

firedragon-goog-safe-download-checkbox =
    .label = Pobieranie skanów
firedragon-goog-safe-download-description = Zezwól Safe Browsing na skanowanie pobieranych plików w celu zidentyfikowania podejrzanych plików. Wszystkie kontrole odbywają się lokalnie.

# Footer
firedragon-footer = Przydatne linki
firedragon-config-link =
    .label = Wszystkie ustawienia zaawansowane (about:config)
firedragon-open-profile =
    .label = Otwórz katalog profilu użytkownika
