## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = αλλαγές στο about:config, λογικά ομαδοποιημένες και εύκολα προσβάσιμες

# Main content
firedragon-header = { -brand-short-name } Προτιμήσεις


firedragon-general-heading = Συμπεριφορά του προγράμματος περιήγησης

firedragon-extension-update-checkbox =
    .label = Αυτόματη ενημέρωση των πρόσθετων
firedragon-extension-update-description = Διατηρήστε τις επεκτάσεις ενημερωμένες χωρίς χειροκίνητη παρέμβαση. Μια καλή επιλογή για την ασφάλειά σας. Εάν δεν ελέγχετε τον κώδικα των επεκτάσεων σας πριν από κάθε ενημέρωση, θα πρέπει να ενεργοποιήσετε αυτή την επιλογή.

firedragon-sync-checkbox =
    .label = Ενεργοποίηση του συγχρονισμού του Firefox
firedragon-sync-description = Συγχρονίστε τα δεδομένα σας με άλλα προγράμματα περιήγησης. Το Firefox Sync κρυπτογραφεί τα δεδομένα τοπικά πριν τα μεταδώσει στον διακομιστή. Απαιτεί επανεκκίνηση.

firedragon-autocopy-checkbox =
    .label = Ενεργοποίηση επικόλλησης με μεσαίο κλικ
firedragon-autocopy-description = Επιλέξτε κάποιο κείμενο για να το αντιγράψετε και, στη συνέχεια, επικολλήστε το με ένα μεσαίο κλικ του ποντικιού.

firedragon-styling-checkbox =
    .label = Επιτρέψτε την προσαρμογή του userChrome.css
firedragon-styling-description = Ενεργοποιήστε αυτό το στοιχείο αν θέλετε να προσαρμόσετε το περιβάλλον εργασίας με ένα θέμα που φορτώνεται χειροκίνητα. Βεβαιωθείτε ότι εμπιστεύεστε τον πάροχο του θέματος.


firedragon-network-heading = Δικτύωση

firedragon-ipv6-checkbox =
    .label = Ενεργοποίηση IPv6
firedragon-ipv6-description = Επιτρέψτε { -brand-short-name } να συνδεθείτε χρησιμοποιώντας IPv6. Αντί να μπλοκάρετε το IPv6 στο πρόγραμμα περιήγησης, σας προτείνουμε να ενεργοποιήσετε την επέκταση απορρήτου IPv6 στο λειτουργικό σας σύστημα.


firedragon-privacy-heading = Απόρρητο

firedragon-xorigin-ref-checkbox =
    .label = Περιορισμός των παραπομπών πολλαπλής προέλευσης
firedragon-xorigin-ref-description = Αποστολή παραπομπής μόνο στην ίδια προέλευση. Αυτό προκαλεί διακοπή. Επιπλέον, ακόμη και όταν αποστέλλονται οι παραπομπές θα εξακολουθούν να κόβονται.


firedragon-broken-heading = Δακτυλικά αποτυπώματα

firedragon-rfp-checkbox =
    .label = Ενεργοποίηση ResistFingerprinting
firedragon-rfp-description = Το ResistFingerprinting είναι το καλύτερο στην κατηγορία του εργαλείο κατά των δακτυλικών αποτυπωμάτων. Εάν πρέπει να το απενεργοποιήσετε, σκεφτείτε να χρησιμοποιήσετε μια επέκταση όπως το Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Ενεργοποίηση letterboxing
firedragon-letterboxing-description = Το Letterboxing εφαρμόζει περιθώρια γύρω από τα παράθυρά σας, προκειμένου να επιστρέψει ένα περιορισμένο σύνολο στρογγυλεμένων αναλύσεων.

firedragon-auto-decline-canvas-checkbox =
    .label = Σιωπηλός αποκλεισμός αιτημάτων πρόσβασης καμβά
firedragon-auto-decline-canvas-description = Αυτόματη άρνηση πρόσβασης του καμβά σε ιστότοπους, χωρίς να ζητείται από τον χρήστη. Είναι ακόμα δυνατό να επιτρέψετε την πρόσβαση στο canvas από τη γραμμή url.

firedragon-webgl-checkbox =
    .label = Ενεργοποίηση WebGL
firedragon-webgl-description = Η WebGL είναι ένας ισχυρός φορέας δακτυλικών αποτυπωμάτων. Εάν πρέπει να την ενεργοποιήσετε, σκεφτείτε να χρησιμοποιήσετε μια επέκταση όπως το Canvas Blocker.


firedragon-security-heading = Ασφάλεια

firedragon-ocsp-checkbox =
    .label = Επιβολή σκληρής αποτυχίας OCSP
firedragon-ocsp-description = Αποτρέψτε τη σύνδεση σε έναν ιστότοπο εάν δεν είναι δυνατή η εκτέλεση του ελέγχου OCSP. Αυτό αυξάνει την ασφάλεια, αλλά θα προκαλέσει διακοπή όταν ένας διακομιστής OCSP είναι εκτός λειτουργίας.

firedragon-goog-safe-checkbox =
    .label = Ενεργοποιήστε την Ασφαλή περιήγηση Google
firedragon-goog-safe-description = Αν ανησυχείτε για κακόβουλο λογισμικό και phishing, σκεφτείτε να το ενεργοποιήσετε. Απενεργοποιημένη λόγω ανησυχιών για λογοκρισία, αλλά συνιστάται για λιγότερο προχωρημένους χρήστες. Όλοι οι έλεγχοι γίνονται τοπικά.

firedragon-goog-safe-download-checkbox =
    .label = Σάρωση λήψεων
firedragon-goog-safe-download-description = Επιτρέψτε στην Ασφαλή περιήγηση να σαρώσει τις λήψεις σας για να εντοπίσει ύποπτα αρχεία. Όλοι οι έλεγχοι γίνονται τοπικά.

# Footer
firedragon-footer = Χρήσιμοι σύνδεσμοι
firedragon-config-link =
    .label = Όλες οι ρυθμίσεις για προχωρημένους (about:config)
firedragon-open-profile =
    .label = Άνοιγμα καταλόγου προφίλ χρήστη
