## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config konfigurasi, dikelompokkan secara logis dan mudah diakses

# Main content
firedragon-header = { -brand-short-name } Preferensi


firedragon-general-heading = Perilaku Peramban

firedragon-extension-update-checkbox =
    .label = Perbarui pengaya secara otomatis
firedragon-extension-update-description = Selalu perbarui ekstensi tanpa intervensi manual. Pilihan yang baik untuk keamanan Anda. Jika Anda tidak meninjau kode ekstensi Anda sebelum setiap pembaruan, Anda harus mengaktifkan opsi ini.

firedragon-sync-checkbox =
    .label = Mengaktifkan Sinkronisasi Firefox
firedragon-sync-description = Menyinkronkan data Anda dengan browser lain. Sinkronisasi Firefox mengenkripsi data secara lokal sebelum mengirimkannya ke server. Memerlukan pengaktifan ulang.

firedragon-autocopy-checkbox =
    .label = Aktifkan tempel klik tengah
firedragon-autocopy-description = Pilih sebagian teks untuk disalin, kemudian tempelkan dengan klik tengah mouse.

firedragon-styling-checkbox =
    .label = Izinkan kustomisasi userChrome.css
firedragon-styling-description = Aktifkan ini jika Anda ingin menyesuaikan UI dengan tema yang dimuat secara manual. Pastikan Anda mempercayai penyedia tema.


firedragon-network-heading = Jaringan

firedragon-ipv6-checkbox =
    .label = Mengaktifkan IPv6
firedragon-ipv6-description = Izinkan { -brand-short-name } untuk terhubung menggunakan IPv6. Daripada memblokir IPv6 di peramban, kami sarankan untuk mengaktifkan ekstensi privasi IPv6 di OS Anda.


firedragon-privacy-heading = Privasi

firedragon-xorigin-ref-checkbox =
    .label = Batasi perujuk lintas asal
firedragon-xorigin-ref-description = Kirim pengarah hanya pada asal yang sama. Hal ini menyebabkan kerusakan. Selain itu, bahkan ketika perujuk yang dikirim masih akan dipangkas.


firedragon-broken-heading = Sidik jari

firedragon-rfp-checkbox =
    .label = Mengaktifkan Sidik Jari Tahan
firedragon-rfp-description = ResistFingerprinting adalah alat anti-sidik jari terbaik di kelasnya. Jika Anda perlu menonaktifkannya, pertimbangkan untuk menggunakan ekstensi seperti Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Mengaktifkan kotak surat
firedragon-letterboxing-description = Letterboxing menerapkan margin di sekeliling jendela Anda, untuk mengembalikan serangkaian resolusi bulat yang terbatas.

firedragon-auto-decline-canvas-checkbox =
    .label = Memblokir permintaan akses kanvas secara diam-diam
firedragon-auto-decline-canvas-description = Secara otomatis menolak akses kanvas ke situs web, tanpa meminta pengguna. Masih memungkinkan untuk mengizinkan akses kanvas dari urlbar.

firedragon-webgl-checkbox =
    .label = Mengaktifkan WebGL
firedragon-webgl-description = WebGL adalah vektor sidik jari yang kuat. Jika Anda perlu mengaktifkannya, pertimbangkan untuk menggunakan ekstensi seperti Canvas Blocker.


firedragon-security-heading = Keamanan

firedragon-ocsp-checkbox =
    .label = Menerapkan kegagalan keras OCSP
firedragon-ocsp-description = Cegah menyambung ke situs web jika pemeriksaan OCSP tidak dapat dilakukan. Hal ini meningkatkan keamanan, tetapi akan menyebabkan kerusakan saat server OCSP mati.

firedragon-goog-safe-checkbox =
    .label = Mengaktifkan Penjelajahan Aman Google
firedragon-goog-safe-description = Jika Anda khawatir tentang malware dan phishing, pertimbangkan untuk mengaktifkannya. Dinonaktifkan karena masalah penyensoran, tetapi direkomendasikan untuk pengguna yang kurang mahir. Semua pemeriksaan dilakukan secara lokal.

firedragon-goog-safe-download-checkbox =
    .label = Memindai unduhan
firedragon-goog-safe-download-description = Izinkan Penjelajahan Aman memindai unduhan Anda untuk mengidentifikasi file yang mencurigakan. Semua pemeriksaan dilakukan secara lokal.

# Footer
firedragon-footer = Tautan yang berguna
firedragon-config-link =
    .label = Semua pengaturan lanjutan (about:config)
firedragon-open-profile =
    .label = Membuka direktori profil pengguna
