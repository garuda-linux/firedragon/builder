## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = modifiche a about:config, raggruppate logicamente e facilmente accessibili

# Main content
firedragon-header = { -brand-short-name } Preferenze


firedragon-general-heading = Comportamento del browser

firedragon-extension-update-checkbox =
    .label = Aggiornamento automatico dei componenti aggiuntivi
firedragon-extension-update-description = Mantenere le estensioni aggiornate senza interventi manuali. Una buona scelta per la vostra sicurezza. Se non rivedete il codice delle vostre estensioni prima di ogni aggiornamento, dovreste attivare questa opzione.

firedragon-sync-checkbox =
    .label = Abilitare la sincronizzazione di Firefox
firedragon-sync-description = Sincronizzazione dei dati con altri browser. Firefox Sync cripta i dati localmente prima di trasmetterli al server. Richiede il riavvio.

firedragon-autocopy-checkbox =
    .label = Abilita l'incollamento con il clic centrale
firedragon-autocopy-description = Selezionare un testo per copiarlo, quindi incollarlo con un clic del mouse centrale.

firedragon-styling-checkbox =
    .label = Permettere la personalizzazione di userChrome.css
firedragon-styling-description = Attivare questa opzione se si desidera personalizzare l'interfaccia utente con un tema caricato manualmente. Assicurarsi di fidarsi del fornitore del tema.


firedragon-network-heading = Collegamento in rete

firedragon-ipv6-checkbox =
    .label = Abilita IPv6
firedragon-ipv6-description = Consentire a { -brand-short-name } di connettersi utilizzando IPv6. Invece di bloccare l'IPv6 nel browser, suggeriamo di abilitare l'estensione della privacy IPv6 nel sistema operativo.


firedragon-privacy-heading = La privacy

firedragon-xorigin-ref-checkbox =
    .label = Limitare i referrer di origine incrociata
firedragon-xorigin-ref-description = Inviare un referrer solo a parità di origine. Questo provoca una rottura. Inoltre, anche quando vengono inviati i referrer, questi vengono comunque tagliati.


firedragon-broken-heading = Impronte digitali

firedragon-rfp-checkbox =
    .label = Abilita ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting è il miglior strumento anti-impronta della categoria. Se avete bisogno di disabilitarlo, prendete in considerazione l'utilizzo di un'estensione come Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Abilita il letterboxing
firedragon-letterboxing-description = Il letterboxing applica dei margini intorno alle finestre, per restituire un insieme limitato di risoluzioni arrotondate.

firedragon-auto-decline-canvas-checkbox =
    .label = Blocco silenzioso delle richieste di accesso al canvas
firedragon-auto-decline-canvas-description = Negare automaticamente l'accesso a canvas ai siti web, senza richiedere l'intervento dell'utente. È ancora possibile consentire l'accesso al canvas dalla barra degli indirizzi.

firedragon-webgl-checkbox =
    .label = Abilitare WebGL
firedragon-webgl-description = WebGL è un forte vettore di fingerprinting. Se dovete abilitarlo, prendete in considerazione l'utilizzo di un'estensione come Canvas Blocker.


firedragon-security-heading = Sicurezza

firedragon-ocsp-checkbox =
    .label = Applicare l'OCSP hard-fail
firedragon-ocsp-description = Impedire la connessione a un sito Web se non è possibile eseguire il controllo OCSP. Questo aumenta la sicurezza, ma causa interruzioni quando un server OCSP è inattivo.

firedragon-goog-safe-checkbox =
    .label = Abilitare la navigazione sicura di Google
firedragon-goog-safe-description = Se siete preoccupati per il malware e il phishing, prendete in considerazione la possibilità di abilitarlo. Disattivato per problemi di censura, ma consigliato agli utenti meno esperti. Tutti i controlli avvengono localmente.

firedragon-goog-safe-download-checkbox =
    .label = Download di scansioni
firedragon-goog-safe-download-description = Consentite a Safe Browsing di eseguire una scansione dei download per identificare i file sospetti. Tutti i controlli avvengono localmente.

# Footer
firedragon-footer = Link utili
firedragon-config-link =
    .label = Tutte le impostazioni avanzate (about:config)
firedragon-open-profile =
    .label = Aprire la directory del profilo utente
