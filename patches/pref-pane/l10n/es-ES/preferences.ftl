## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = cambios about:config, agrupados de forma lógica y fácilmente accesibles

# Main content
firedragon-header = Preferencias { -brand-short-name }


firedragon-general-heading = Browser Behavior

firedragon-extension-update-checkbox =
    .label = Comportamiento del navegador
firedragon-extension-update-description = Mantenga las extensiones actualizadas sin intervención manual. Una buena opción para su seguridad. Si no revisas el código de tus extensiones antes de cada actualización, deberías activar esta opción.

firedragon-sync-checkbox =
    .label = Activar Firefox Sync
firedragon-sync-description = Sincroniza tus datos con otros navegadores. Firefox Sync encripta los datos localmente antes de transmitirlos al servidor. Requiere reinicio.

firedragon-autocopy-checkbox =
    .label = Activar pegar con el botón central
firedragon-autocopy-description = Selecciona un texto para copiarlo y pégalo con el botón central del ratón.

firedragon-styling-checkbox =
    .label = Permitir la personalización de userChrome.css
firedragon-styling-description = Active esta opción si desea personalizar la interfaz de usuario con un tema cargado manualmente. Asegúrate de que confías en el proveedor del tema.


firedragon-network-heading = Red

firedragon-ipv6-checkbox =
    .label = Activar IPv6
firedragon-ipv6-description = Permitir { -brand-short-name } conectarse usando IPv6. En lugar de bloquear IPv6 en el navegador, le sugerimos que active la extensión de privacidad IPv6 en su sistema operativo.


firedragon-privacy-heading = Privacidad

firedragon-xorigin-ref-checkbox =
    .label = Limitar los remitentes de origen cruzado
firedragon-xorigin-ref-description = Enviar un referrer sólo en same-origin. Esto provoca roturas. Además, incluso cuando se envían los remitentes seguirán siendo recortados.


firedragon-broken-heading = Huellas dactilares

firedragon-rfp-checkbox =
    .label = Activar ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting es la mejor herramienta antihuellas de su clase. Si necesitas desactivarla, considera usar una extensión como Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Activar el buzón
firedragon-letterboxing-description = Letterboxing aplica márgenes alrededor de sus ventanas, con el fin de devolver un conjunto limitado de resoluciones redondeadas.

firedragon-auto-decline-canvas-checkbox =
    .label = Bloquear silenciosamente las solicitudes de acceso a canvas
firedragon-auto-decline-canvas-description = Denegar automáticamente el acceso de canvas a sitios web, sin preguntar al usuario. Aún es posible permitir el acceso a canvas desde la urlbar.

firedragon-webgl-checkbox =
    .label = Activar WebGL
firedragon-webgl-description = WebGL es un fuerte vector de fingerprinting. Si necesitas habilitarlo, considera usar una extensión como Canvas Blocker.


firedragon-security-heading = Seguridad

firedragon-ocsp-checkbox =
    .label = Aplicar OCSP hard-fail
firedragon-ocsp-description = Impedir la conexión a un sitio web si no se puede realizar la comprobación OCSP. Esto aumenta la seguridad, pero causará interrupciones cuando un servidor OCSP esté caído.

firedragon-goog-safe-checkbox =
    .label = Habilitar la navegación segura de Google
firedragon-goog-safe-description = Si te preocupan el malware y el phishing, considera activarlo. Desactivado por motivos de censura, pero recomendado para usuarios menos avanzados. Todas las comprobaciones se realizan localmente.

firedragon-goog-safe-download-checkbox =
    .label = Escanear descargas
firedragon-goog-safe-download-description = Permita que la Navegación Segura analice sus descargas para identificar archivos sospechosos. Todas las comprobaciones se realizan localmente.

# Footer
firedragon-footer = Enlaces útiles
firedragon-config-link =
    .label = Todos los ajustes avanzados (about:config)
firedragon-open-profile =
    .label = Abrir el directorio de perfiles de usuario
