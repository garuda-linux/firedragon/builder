## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = การเปลี่ยนแปลง about:config จัดกลุ่มตามตรรกะและเข้าถึงได้ง่าย

# Main content
firedragon-header = { -brand-short-name } การตั้งค่า


firedragon-general-heading = พฤติกรรมของเบราว์เซอร์

firedragon-extension-update-checkbox =
    .label = อัปเดตส่วนเสริมโดยอัตโนมัติ
firedragon-extension-update-description = อัปเดตส่วนขยายให้ทันสมัยอยู่เสมอโดยไม่ต้องมีการแทรกแซงด้วยตนเอง ทางเลือกที่ดีสำหรับความปลอดภัยของคุณ หากคุณไม่ตรวจสอบโค้ดของส่วนขยายก่อนการอัปเดตทุกครั้ง คุณควรเปิดใช้งานตัวเลือกนี้

firedragon-sync-checkbox =
    .label = เปิดใช้งาน Firefox Sync
firedragon-sync-description = ซิงค์ข้อมูลของคุณกับเบราว์เซอร์อื่น Firefox Sync เข้ารหัสข้อมูลภายในเครื่องก่อนที่จะส่งข้อมูลไปยังเซิร์ฟเวอร์ ต้องรีสตาร์ท

firedragon-autocopy-checkbox =
    .label = เปิดใช้งานการวางคลิกกลาง
firedragon-autocopy-description = เลือกข้อความที่จะคัดลอก จากนั้นวางโดยคลิกเมาส์กลาง

firedragon-styling-checkbox =
    .label = อนุญาตการปรับแต่ง userChrome.css
firedragon-styling-description = เปิดใช้งานสิ่งนี้หากคุณต้องการปรับแต่ง UI ด้วยธีมที่โหลดด้วยตนเอง ตรวจสอบให้แน่ใจว่าคุณเชื่อถือผู้ให้บริการธีม


firedragon-network-heading = เครือข่าย

firedragon-ipv6-checkbox =
    .label = เปิดใช้งาน IPv6
firedragon-ipv6-description = อนุญาตให้ { -brand-short-name } เชื่อมต่อโดยใช้ IPv6 แทนที่จะบล็อก IPv6 ในเบราว์เซอร์ เราขอแนะนำให้เปิดใช้งานส่วนขยายความเป็นส่วนตัว IPv6 ในระบบปฏิบัติการของคุณ


firedragon-privacy-heading = ความเป็นส่วนตัว

firedragon-xorigin-ref-checkbox =
    .label = จำกัดผู้อ้างอิงข้ามต้นทาง
firedragon-xorigin-ref-description = ส่งผู้อ้างอิงเฉพาะในแหล่งที่มาเดียวกันเท่านั้น สิ่งนี้ทำให้เกิดการแตกหัก นอกจากนี้ แม้ว่าผู้อ้างอิงที่ส่งไปแล้วจะยังคงถูกตัดออก


firedragon-broken-heading = ลายนิ้วมือ

firedragon-rfp-checkbox =
    .label = เปิดใช้งาน ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting เป็นเครื่องมือป้องกันลายนิ้วมือที่ดีที่สุดในระดับเดียวกัน หากคุณต้องการปิดการใช้งาน ให้ลองใช้ส่วนขยาย เช่น Canvas Blocker

firedragon-letterboxing-checkbox =
    .label = เปิดใช้งานกล่องจดหมาย
firedragon-letterboxing-description = แถบดำแถบดำจะใช้ระยะขอบรอบๆ หน้าต่างของคุณ เพื่อแสดงชุดความละเอียดแบบโค้งมนที่จำกัด

firedragon-auto-decline-canvas-checkbox =
    .label = บล็อกคำขอเข้าถึง Canvas โดยไม่แจ้งให้ทราบ
firedragon-auto-decline-canvas-description = ปฏิเสธการเข้าถึงเว็บไซต์ Canvas โดยอัตโนมัติ โดยไม่ต้องแจ้งผู้ใช้ ยังคงสามารถอนุญาตให้เข้าถึง Canvas ได้จากแถบ URL

firedragon-webgl-checkbox =
    .label = เปิดใช้งาน WebGL
firedragon-webgl-description = WebGL เป็นเวกเตอร์ลายนิ้วมือที่แข็งแกร่ง หากคุณต้องการเปิดใช้งาน ให้ลองใช้ส่วนขยาย เช่น Canvas Blocker


firedragon-security-heading = ความปลอดภัย

firedragon-ocsp-checkbox =
    .label = บังคับใช้ความล้มเหลวอย่างหนักของ OCSP
firedragon-ocsp-description = ป้องกันการเชื่อมต่อกับเว็บไซต์หากไม่สามารถดำเนินการตรวจสอบ OCSP ได้ สิ่งนี้จะเพิ่มความปลอดภัย แต่จะทำให้เกิดการเสียหายเมื่อเซิร์ฟเวอร์ OCSP หยุดทำงาน

firedragon-goog-safe-checkbox =
    .label = เปิดใช้งาน Google Safe Browsing
firedragon-goog-safe-description = หากคุณกังวลเกี่ยวกับมัลแวร์และฟิชชิ่ง ให้พิจารณาเปิดใช้งาน ปิดใช้งานเนื่องจากข้อกังวลเกี่ยวกับการเซ็นเซอร์ แต่แนะนำสำหรับผู้ใช้ขั้นสูงน้อยกว่า การตรวจสอบทั้งหมดเกิดขึ้นในท้องถิ่น

firedragon-goog-safe-download-checkbox =
    .label = สแกนการดาวน์โหลด
firedragon-goog-safe-download-description = อนุญาตให้ Google Safe Browsing สแกนการดาวน์โหลดของคุณเพื่อระบุไฟล์ที่น่าสงสัย การตรวจสอบทั้งหมดเกิดขึ้นในท้องถิ่น

# Footer
firedragon-footer = ลิงค์ที่มีประโยชน์
firedragon-config-link =
    .label = การตั้งค่าขั้นสูงทั้งหมด (about:config)
firedragon-open-profile =
    .label = เปิดไดเร็กทอรีโปรไฟล์ผู้ใช้
