## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config thay đổi, được nhóm hợp lý và dễ truy cập

# Main content
firedragon-header = Sở thích { -brand-short-name }


firedragon-general-heading = Hành vi của trình duyệt

firedragon-extension-update-checkbox =
    .label = Tự động cập nhật tiện ích bổ sung
firedragon-extension-update-description = Giữ cho tiện ích mở rộng được cập nhật mà không cần can thiệp thủ công. Một lựa chọn tốt cho bảo mật của bạn. Nếu bạn không xem lại mã của tiện ích mở rộng trước mỗi lần cập nhật, bạn nên bật tùy chọn này.

firedragon-sync-checkbox =
    .label = Bật Firefox Sync
firedragon-sync-description = Đồng bộ dữ liệu của bạn với các trình duyệt khác. Firefox Sync mã hóa dữ liệu cục bộ trước khi truyền đến máy chủ. Cần khởi động lại.

firedragon-autocopy-checkbox =
    .label = Bật chức năng dán bằng cách nhấp chuột giữa
firedragon-autocopy-description = Chọn một số văn bản để sao chép, sau đó dán bằng cách nhấp chuột giữa.

firedragon-styling-checkbox =
    .label = Cho phép tùy chỉnh userChrome.css
firedragon-styling-description = Bật tùy chọn này nếu bạn muốn tùy chỉnh giao diện người dùng bằng chủ đề được tải thủ công. Đảm bảo bạn tin tưởng nhà cung cấp chủ đề.


firedragon-network-heading = Mạng lưới

firedragon-ipv6-checkbox =
    .label = Bật IPv6
firedragon-ipv6-description = Cho phép { -brand-short-name } kết nối bằng IPv6. Thay vì chặn IPv6 trong trình duyệt, chúng tôi khuyên bạn nên bật tiện ích mở rộng quyền riêng tư IPv6 trong hệ điều hành của bạn.


firedragon-privacy-heading = Sự riêng tư

firedragon-xorigin-ref-checkbox =
    .label = Giới hạn người giới thiệu nguồn gốc chéo
firedragon-xorigin-ref-description = Chỉ gửi người giới thiệu trên cùng một nguồn gốc. Điều này gây ra sự cố. Ngoài ra, ngay cả khi đã gửi người giới thiệu vẫn sẽ bị cắt bớt.


firedragon-broken-heading = Lấy dấu vân tay

firedragon-rfp-checkbox =
    .label = Bật tính năng Chống vân tay
firedragon-rfp-description = ResistFingerprinting là công cụ chống dấu vân tay tốt nhất trong lớp. Nếu bạn cần vô hiệu hóa nó, hãy cân nhắc sử dụng tiện ích mở rộng như Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Bật hộp thư
firedragon-letterboxing-description = Letterboxing áp dụng lề xung quanh cửa sổ của bạn để trả về một tập hợp giới hạn các độ phân giải tròn.

firedragon-auto-decline-canvas-checkbox =
    .label = Chặn các yêu cầu truy cập canvas một cách âm thầm
firedragon-auto-decline-canvas-description = Tự động từ chối quyền truy cập canvas vào các trang web mà không cần nhắc nhở người dùng. Vẫn có thể cho phép truy cập canvas từ thanh url.

firedragon-webgl-checkbox =
    .label = Bật WebGL
firedragon-webgl-description = WebGL là một vector dấu vân tay mạnh. Nếu bạn cần bật nó, hãy cân nhắc sử dụng tiện ích mở rộng như Canvas Blocker.


firedragon-security-heading = Bảo vệ

firedragon-ocsp-checkbox =
    .label = Thực thi lỗi cứng OCSP
firedragon-ocsp-description = Ngăn chặn kết nối đến một trang web nếu không thể thực hiện kiểm tra OCSP. Điều này làm tăng tính bảo mật, nhưng sẽ gây ra sự cố khi máy chủ OCSP ngừng hoạt động.

firedragon-goog-safe-checkbox =
    .label = Bật tính năng Duyệt web an toàn của Google
firedragon-goog-safe-description = Nếu bạn lo lắng về phần mềm độc hại và lừa đảo, hãy cân nhắc bật tính năng này. Tắt vì lo ngại kiểm duyệt nhưng khuyến nghị cho người dùng ít kinh nghiệm hơn. Tất cả các lần kiểm tra đều diễn ra cục bộ.

firedragon-goog-safe-download-checkbox =
    .label = Quét tải xuống
firedragon-goog-safe-download-description = Cho phép Safe Browsing quét các tệp tải xuống của bạn để xác định các tệp đáng ngờ. Tất cả các lần kiểm tra đều diễn ra cục bộ.

# Footer
firedragon-footer = Liên kết hữu ích
firedragon-config-link =
    .label = Tất cả các thiết lập nâng cao (about:config)
firedragon-open-profile =
    .label = Mở thư mục hồ sơ người dùng
