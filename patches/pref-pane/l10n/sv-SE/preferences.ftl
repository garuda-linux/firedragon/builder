## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config-ändringar, logiskt grupperade och lättillgängliga

# Main content
firedragon-header = { -brand-short-name } Inställningar


firedragon-general-heading = Beteende i webbläsare

firedragon-extension-update-checkbox =
    .label = Uppdatera tillägg automatiskt
firedragon-extension-update-description = Håller tillägg uppdaterade utan manuella ingrepp. Ett bra val för din säkerhet. Om du inte granskar koden för dina tillägg före varje uppdatering bör du aktivera det här alternativet.

firedragon-sync-checkbox =
    .label = Aktivera Firefox Sync
firedragon-sync-description = Synkronisera dina data med andra webbläsare. Firefox Sync krypterar data lokalt innan de överförs till servern. Kräver omstart.

firedragon-autocopy-checkbox =
    .label = Aktivera klistra in med mittklick
firedragon-autocopy-description = Markera en text för att kopiera den och klistra sedan in den med ett musklick i mitten.

firedragon-styling-checkbox =
    .label = Tillåt anpassning av userChrome.css
firedragon-styling-description = Aktivera detta om du vill anpassa användargränssnittet med ett manuellt laddat tema. Se till att du litar på leverantören av temat.


firedragon-network-heading = Nätverkande

firedragon-ipv6-checkbox =
    .label = Aktivera IPv6
firedragon-ipv6-description = Tillåt { -brand-short-name } att ansluta med IPv6. Istället för att blockera IPv6 i webbläsaren föreslår vi att du aktiverar IPv6-sekretesstillägget i ditt operativsystem.


firedragon-privacy-heading = Integritet

firedragon-xorigin-ref-checkbox =
    .label = Begränsa hänvisningar mellan olika ursprung
firedragon-xorigin-ref-description = Skicka en referrer endast på samma ursprung. Detta orsakar brott. Dessutom kommer referenser fortfarande att trimmas även när de skickas.


firedragon-broken-heading = Fingeravtryck

firedragon-rfp-checkbox =
    .label = Aktivera MotståFingeravtryck
firedragon-rfp-description = ResistFingerprinting är det bästa anti-fingerprinting-verktyget i sin klass. Om du behöver inaktivera det kan du överväga att använda ett tillägg som Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Aktivera letterboxing
firedragon-letterboxing-description = Letterboxing tillämpar marginaler runt dina fönster för att returnera en begränsad uppsättning avrundade upplösningar.

firedragon-auto-decline-canvas-checkbox =
    .label = Tyst blockera begäran om åtkomst till canvas
firedragon-auto-decline-canvas-description = Neka automatiskt canvas-åtkomst till webbplatser utan att fråga användaren. Det är fortfarande möjligt att tillåta canvas-åtkomst från url-fältet.

firedragon-webgl-checkbox =
    .label = Aktivera WebGL
firedragon-webgl-description = WebGL är en stark fingeravtrycksvektor. Om du behöver aktivera det kan du överväga att använda ett tillägg som Canvas Blocker.


firedragon-security-heading = Säkerhet

firedragon-ocsp-checkbox =
    .label = Genomföra OCSP hard-fail
firedragon-ocsp-description = Förhindra anslutning till en webbplats om OCSP-kontrollen inte kan utföras. Detta ökar säkerheten, men det kommer att orsaka avbrott när en OCSP-server ligger nere.

firedragon-goog-safe-checkbox =
    .label = Aktivera Google Safe Browsing
firedragon-goog-safe-description = Om du är orolig för skadlig kod och nätfiske bör du överväga att aktivera det. Avaktiverad på grund av censurproblem, men rekommenderas för mindre avancerade användare. Alla kontroller sker lokalt.

firedragon-goog-safe-download-checkbox =
    .label = Skanna nedladdningar
firedragon-goog-safe-download-description = Låt Safe Browsing skanna dina nedladdningar för att identifiera misstänkta filer. Alla kontroller sker lokalt.

# Footer
firedragon-footer = Användbara länkar
firedragon-config-link =
    .label = Alla avancerade inställningar (about:config)
firedragon-open-profile =
    .label = Öppna användarprofilens katalog
