## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config változások, logikusan csoportosítva és könnyen hozzáférhetően

# Main content
firedragon-header = { -brand-short-name } Beállítások


firedragon-general-heading = Böngésző viselkedés

firedragon-extension-update-checkbox =
    .label = A bővítmények automatikus frissítése
firedragon-extension-update-description = Kézi beavatkozás nélkül naprakészen tarthatja a bővítményeket. Jó választás az Ön biztonsága érdekében. Ha nem vizsgálja felül a bővítményei kódját minden frissítés előtt, akkor érdemes engedélyeznie ezt a beállítást.

firedragon-sync-checkbox =
    .label = Firefox szinkronizálás engedélyezése
firedragon-sync-description = Szinkronizálja adatait más böngészőkkel. A Firefox Sync titkosítja az adatokat helyben, mielőtt továbbítja azokat a szerverre. Újraindítást igényel.

firedragon-autocopy-checkbox =
    .label = Középső kattintással történő beillesztés engedélyezése
firedragon-autocopy-description = Jelöljön ki egy szöveget a másoláshoz, majd illessze be az egér középső gombjával.

firedragon-styling-checkbox =
    .label = A userChrome.css testreszabásának engedélyezése
firedragon-styling-description = Engedélyezze ezt, ha a felhasználói felületet kézzel betöltött témával szeretné testreszabni. Győződjön meg róla, hogy megbízik a téma szolgáltatójában.


firedragon-network-heading = Hálózatépítés

firedragon-ipv6-checkbox =
    .label = IPv6 engedélyezése
firedragon-ipv6-description = Engedélyezze a { -brand-short-name } IPv6 használatával történő csatlakozást. Az IPv6 böngészőben történő blokkolása helyett javasoljuk, hogy engedélyezze az IPv6 adatvédelmi kiterjesztést az operációs rendszerben.


firedragon-privacy-heading = Adatvédelem

firedragon-xorigin-ref-checkbox =
    .label = Korlátozza a cross-origin referrereket
firedragon-xorigin-ref-description = Csak azonos eredetű hivatkozás küldése. Ez törést okoz. Ezen kívül még akkor is, ha a referrereket elküldjük, akkor is levágásra kerülnek.


firedragon-broken-heading = Ujjlenyomatvétel

firedragon-rfp-checkbox =
    .label = Enable ResistFingerprinting engedélyezése
firedragon-rfp-description = A ResistFingerprinting a kategóriájában legjobb ujjlenyomat-ellenes eszköz. Ha le kell tiltania, fontolja meg egy olyan kiterjesztés használatát, mint a Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Letterboxing engedélyezése
firedragon-letterboxing-description = A Letterboxing margókat alkalmaz az ablakok körül, hogy egy korlátozott számú lekerekített felbontást adjon vissza.

firedragon-auto-decline-canvas-checkbox =
    .label = A vászon hozzáférési kérelmek csendes blokkolása
firedragon-auto-decline-canvas-description = Automatikusan, a felhasználó kérdezése nélkül megtagadhatja a vászonhoz való hozzáférést a webhelyekhez. A canvas-hozzáférés engedélyezése az url-bárból továbbra is lehetséges.

firedragon-webgl-checkbox =
    .label = WebGL engedélyezése
firedragon-webgl-description = A WebGL egy erős ujjlenyomatvektor. Ha engedélyeznie kell, fontolja meg egy olyan kiterjesztés használatát, mint a Canvas Blocker.


firedragon-security-heading = Biztonság

firedragon-ocsp-checkbox =
    .label = OCSP hard-fail kényszerítése
firedragon-ocsp-description = A webhelyhez való csatlakozás megakadályozása, ha az OCSP-ellenőrzés nem végezhető el. Ez növeli a biztonságot, de törést okoz, ha egy OCSP-kiszolgáló nem működik.

firedragon-goog-safe-checkbox =
    .label = A Google biztonságos böngészés engedélyezése
firedragon-goog-safe-description = Ha aggódik a rosszindulatú szoftverek és az adathalászat miatt, fontolja meg az engedélyezését. Cenzúrával kapcsolatos aggályok miatt letiltva, de kevésbé fejlett felhasználóknak ajánlott. Minden ellenőrzés helyben történik.

firedragon-goog-safe-download-checkbox =
    .label = Letöltések beolvasása
firedragon-goog-safe-download-description = Engedélyezze, hogy a Biztonságos böngészés átvizsgálja a letöltéseket a gyanús fájlok azonosítása érdekében. Minden ellenőrzés helyben történik.

# Footer
firedragon-footer = Hasznos linkek
firedragon-config-link =
    .label = Minden speciális beállítás (about:config)
firedragon-open-profile =
    .label = Felhasználói profil könyvtár megnyitása
