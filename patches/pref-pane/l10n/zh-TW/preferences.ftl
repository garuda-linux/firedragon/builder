## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config 更改，邏輯分組且易於訪問

# Main content
firedragon-header = { -brand-short-name } 偏好設定


firedragon-general-heading = 瀏覽器行為

firedragon-extension-update-checkbox =
    .label = 自動更新附加元件
firedragon-extension-update-description = 無需手動幹預即可保持擴充功能處於最新狀態。 為了您的安全，這是一個不錯的選擇。 如果您在每次更新之前不檢查擴充功能的程式碼，則應啟用此選項。

firedragon-sync-checkbox =
    .label = 啟用 Firefox 同步
firedragon-sync-description = 與其他瀏覽器同步您的資料。 Firefox Sync 在將資料傳輸到伺服器之前會在本地對其進行加密。 需要重新啟動。

firedragon-autocopy-checkbox =
    .label = 啟用中鍵點選貼上
firedragon-autocopy-description = 選擇一些文字進行複製，然後按一下滑鼠中鍵進行貼上。

firedragon-styling-checkbox =
    .label = 允許 userChrome.css 自訂
firedragon-styling-description = 如果您想使用手動載入的主題自訂 UI，請啟用此選項。 確保您信任主題的提供者。


firedragon-network-heading = 聯網

firedragon-ipv6-checkbox =
    .label = 啟用 IPv6
firedragon-ipv6-description = 允許 { -brand-short-name } 使用 IPv6 連線。 我們建議您在作業系統中啟用 IPv6 隱私擴展，而不是在瀏覽器中封鎖 IPv6。


firedragon-privacy-heading = 隱私

firedragon-xorigin-ref-checkbox =
    .label = 限制跨域引用
firedragon-xorigin-ref-description = 僅在同源上發送引薦來源網址。 這會導致破損。 此外，即使發送了推薦人，仍然會被修剪。


firedragon-broken-heading = 指紋辨識

firedragon-rfp-checkbox =
    .label = 啟用抗指紋識別
firedragon-rfp-description = ResistFingerprinting 是同類中最好的防指紋工具。 如果您需要停用它，請考慮使用 Canvas Blocker 等擴充功能。

firedragon-letterboxing-checkbox =
    .label = 啟用信箱
firedragon-letterboxing-description = 信箱在視窗周圍套用邊距，以便傳回一組有限的捨入解析度。

firedragon-auto-decline-canvas-checkbox =
    .label = 靜默阻止畫布存取請求
firedragon-auto-decline-canvas-description = 自動拒絕畫布造訪網站，而不提示使用者。 仍然可以允許從 URL 欄存取畫布。

firedragon-webgl-checkbox =
    .label = 啟用 WebGL
firedragon-webgl-description = WebGL 是一個強大的指紋辨識向量。 如果您需要啟用它，請考慮使用 Canvas Blocker 等擴充功能。


firedragon-security-heading = 安全

firedragon-ocsp-checkbox =
    .label = 強制執行 OCSP 硬故障
firedragon-ocsp-description = 如果無法執行 OCSP 檢查，則阻止連線到網站。 這提高了安全性，但當 OCSP 伺服器關閉時會導致損壞。

firedragon-goog-safe-checkbox =
    .label = 啟用 Google 安全瀏覽
firedragon-goog-safe-description = 如果您擔心惡意軟體和網路釣魚，請考慮啟用它。 因審查問題而被禁用，但建議不太高級的用戶使用。 所有檢查都在本地進行。

firedragon-goog-safe-download-checkbox =
    .label = 掃描下載
firedragon-goog-safe-download-description = 允許安全瀏覽掃描您的下載以識別可疑檔案。 所有檢查都在本地進行。

# Footer
firedragon-footer = 有用的連結
firedragon-config-link =
    .label = 所有進階設定 (about:config)
firedragon-open-profile =
    .label = 開啟使用者設定檔目錄
