## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config 更改，逻辑分组且易于访问

# Main content
firedragon-header = { -brand-short-name } 偏好设置


firedragon-general-heading = 浏览器行为

firedragon-extension-update-checkbox =
    .label = 自动更新附加组件
firedragon-extension-update-description = 无需手动干预即可保持扩展程序处于最新状态。 为了您的安全，这是一个不错的选择。 如果您在每次更新之前不检查扩展程序的代码，则应启用此选项。

firedragon-sync-checkbox =
    .label = 启用 Firefox 同步
firedragon-sync-description = 与其他浏览器同步您的数据。 Firefox Sync 在将数据传输到服务器之前会在本地对其进行加密。 需要重新启动。

firedragon-autocopy-checkbox =
    .label = 启用中键单击粘贴
firedragon-autocopy-description = 选择一些文本进行复制，然后单击鼠标中键进行粘贴。

firedragon-styling-checkbox =
    .label = 允许 userChrome.css 自定义
firedragon-styling-description = 如果您想使用手动加载的主题自定义 UI，请启用此选项。 确保您信任主题的提供者。


firedragon-network-heading = 联网

firedragon-ipv6-checkbox =
    .label = 启用 IPv6
firedragon-ipv6-description = 允许 { -brand-short-name } 使用 IPv6 进行连接。 我们建议您在操作系统中启用 IPv6 隐私扩展，而不是在浏览器中阻止 IPv6。


firedragon-privacy-heading = 隐私

firedragon-xorigin-ref-checkbox =
    .label = 限制跨域引用
firedragon-xorigin-ref-description = 仅在同源上发送引荐来源网址。 这会导致破损。 此外，即使发送了推荐人，仍然会被修剪。


firedragon-broken-heading = 指纹识别

firedragon-rfp-checkbox =
    .label = 启用抗指纹识别
firedragon-rfp-description = ResistFingerprinting 是同类中最好的防指纹工具。 如果您需要禁用它，请考虑使用 Canvas Blocker 等扩展。

firedragon-letterboxing-checkbox =
    .label = 启用信箱
firedragon-letterboxing-description = 信箱在窗口周围应用边距，以便返回一组有限的舍入分辨率。

firedragon-auto-decline-canvas-checkbox =
    .label = 静默阻止画布访问请求
firedragon-auto-decline-canvas-description = 自动拒绝画布访问网站，而不提示用户。 仍然可以允许从 URL 栏访问画布。

firedragon-webgl-checkbox =
    .label = 启用 WebGL
firedragon-webgl-description = WebGL 是一个强大的指纹识别向量。 如果您需要启用它，请考虑使用 Canvas Blocker 等扩展。


firedragon-security-heading = 安全

firedragon-ocsp-checkbox =
    .label = 强制执行 OCSP 硬故障
firedragon-ocsp-description = 如果无法执行 OCSP 检查，则阻止连接到网站。 这提高了安全性，但当 OCSP 服务器关闭时会导致损坏。

firedragon-goog-safe-checkbox =
    .label = 启用 Google 安全浏览
firedragon-goog-safe-description = 如果您担心恶意软件和网络钓鱼，请考虑启用它。 因审查问题而被禁用，但建议不太高级的用户使用。 所有检查都在本地进行。

firedragon-goog-safe-download-checkbox =
    .label = 扫描下载
firedragon-goog-safe-download-description = 允许安全浏览扫描您的下载以识别可疑文件。 所有检查都在本地进行。

# Footer
firedragon-footer = 有用的链接
firedragon-config-link =
    .label = 所有高级设置 (about:config)
firedragon-open-profile =
    .label = 打开用户配置文件目录
