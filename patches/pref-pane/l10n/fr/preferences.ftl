## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = les modifications apportées à about:config, regroupées de manière logique et facilement accessibles

# Main content
firedragon-header = { -brand-short-name } Préférences


firedragon-general-heading = Comportement du navigateur

firedragon-extension-update-checkbox =
    .label = Mise à jour automatique des modules complémentaires
firedragon-extension-update-description = Maintenir les extensions à jour sans intervention manuelle. Un bon choix pour votre sécurité. Si vous n'examinez pas le code de vos extensions avant chaque mise à jour, vous devriez activer cette option.

firedragon-sync-checkbox =
    .label = Activer Firefox Sync
firedragon-sync-description = Synchronisez vos données avec d'autres navigateurs. Firefox Sync crypte les données localement avant de les transmettre au serveur. Nécessite un redémarrage.

firedragon-autocopy-checkbox =
    .label = Activer le collage par le clic du milieu
firedragon-autocopy-description = Sélectionnez du texte pour le copier, puis collez-le en cliquant sur le bouton du milieu de la souris.

firedragon-styling-checkbox =
    .label = Autoriser la personnalisation de userChrome.css
firedragon-styling-description = Activez cette option si vous souhaitez personnaliser l'interface utilisateur à l'aide d'un thème chargé manuellement. Assurez-vous de faire confiance au fournisseur du thème.


firedragon-network-heading = Mise en réseau

firedragon-ipv6-checkbox =
    .label = Activer IPv6
firedragon-ipv6-description = Autoriser { -brand-short-name } à se connecter en utilisant IPv6. Au lieu de bloquer IPv6 dans le navigateur, nous suggérons d'activer l'extension de confidentialité IPv6 dans votre système d'exploitation.


firedragon-privacy-heading = Vie privée

firedragon-xorigin-ref-checkbox =
    .label = Limiter les renvois d'origine croisée
firedragon-xorigin-ref-description = Envoyer un référent uniquement en cas d'origine identique. Cela provoque des ruptures. De plus, même si les référents sont envoyés, ils seront toujours coupés.


firedragon-broken-heading = Prise d'empreintes digitales

firedragon-rfp-checkbox =
    .label = Activer ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting est le meilleur outil anti-fingerprinting de sa catégorie. Si vous devez le désactiver, envisagez d'utiliser une extension comme Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Activer le letterboxing
firedragon-letterboxing-description = Le Letterboxing applique des marges autour de vos fenêtres, afin de renvoyer un ensemble limité de résolutions arrondies.

firedragon-auto-decline-canvas-checkbox =
    .label = Bloquer silencieusement les demandes d'accès à la toile
firedragon-auto-decline-canvas-description = Refuser automatiquement l'accès de canvas aux sites web, sans demander à l'utilisateur de le faire. Il est toujours possible d'autoriser l'accès au canevas à partir de la barre d'url.

firedragon-webgl-checkbox =
    .label = Activer WebGL
firedragon-webgl-description = WebGL est un puissant vecteur d'empreintes digitales. Si vous devez l'activer, envisagez d'utiliser une extension comme Canvas Blocker.


firedragon-security-heading = Sécurité

firedragon-ocsp-checkbox =
    .label = Appliquer le principe de l'échec pur et simple de l'OCSP
firedragon-ocsp-description = Empêcher la connexion à un site web si la vérification OCSP ne peut être effectuée. Cela augmente la sécurité, mais provoque des pannes lorsqu'un serveur OCSP est en panne.

firedragon-goog-safe-checkbox =
    .label = Activer la navigation sécurisée de Google
firedragon-goog-safe-description = Si vous êtes préoccupé par les logiciels malveillants et l'hameçonnage, envisagez de l'activer. Désactivé pour des raisons de censure, mais recommandé pour les utilisateurs moins avancés. Toutes les vérifications sont effectuées localement.

firedragon-goog-safe-download-checkbox =
    .label = Scanner les téléchargements
firedragon-goog-safe-download-description = Permettez à Safe Browsing d'analyser vos téléchargements afin d'identifier les fichiers suspects. Toutes les vérifications sont effectuées localement.

# Footer
firedragon-footer = Liens utiles
firedragon-config-link =
    .label = Tous les paramètres avancés (about:config)
firedragon-open-profile =
    .label = Ouvrir le répertoire du profil de l'utilisateur
