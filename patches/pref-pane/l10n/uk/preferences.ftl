## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config змін, логічно згрупованих та легкодоступних

# Main content
firedragon-header = { -brand-short-name } Уподобання


firedragon-general-heading = Поведінка браузера

firedragon-extension-update-checkbox =
    .label = Автоматично оновлювати доповнення
firedragon-extension-update-description = Підтримуйте розширення в актуальному стані без ручного втручання. Хороший вибір для вашої безпеки. Якщо ви не перевіряєте код розширень перед кожним оновленням, вам слід увімкнути цю опцію.

firedragon-sync-checkbox =
    .label = Увімкнути синхронізацію Firefox
firedragon-sync-description = Синхронізація даних з іншими браузерами. Firefox Sync шифрує дані локально, перш ніж передавати їх на сервер. Потребує перезапуску.

firedragon-autocopy-checkbox =
    .label = Ввімкнути вставку середнім клацанням миші
firedragon-autocopy-description = Виділіть текст, щоб скопіювати його, а потім вставте його середнім клацанням миші.

firedragon-styling-checkbox =
    .label = Дозволити налаштування userChrome.css
firedragon-styling-description = Увімкніть цю опцію, якщо ви хочете налаштувати інтерфейс за допомогою теми, завантаженої вручну. Переконайтеся, що ви довіряєте постачальнику теми.


firedragon-network-heading = Нетворкінг

firedragon-ipv6-checkbox =
    .label = Увімкнути IPv6
firedragon-ipv6-description = Дозвольте { -brand-short-name } підключатися за протоколом IPv6. Замість того, щоб блокувати IPv6 у браузері, ми рекомендуємо увімкнути розширення конфіденційності IPv6 у вашій операційній системі.


firedragon-privacy-heading = Конфіденційність

firedragon-xorigin-ref-checkbox =
    .label = Обмежити перехресне походження рефералів
firedragon-xorigin-ref-description = Надсилайте реферала тільки на одноіменні посилання. Це призводить до поломки. Крім того, навіть після відправки реферали все одно будуть обрізані.


firedragon-broken-heading = Зняття відбитків пальців

firedragon-rfp-checkbox =
    .label = Ввімкнути ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting - найкращий у своєму класі інструмент для боротьби з відбитками пальців. Якщо вам потрібно його вимкнути, скористайтеся розширенням на кшталт Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Увімкнути поштову скриньку
firedragon-letterboxing-description = Поштова скринька застосовує поля навколо ваших вікон, щоб повернути обмежений набір округлих роздільних здатностей.

firedragon-auto-decline-canvas-checkbox =
    .label = Тихе блокування запитів на доступ до полотна
firedragon-auto-decline-canvas-description = Автоматично забороняти доступ до полотна на веб-сайтах без запиту користувача. Досі можна дозволити доступ до полотна з url-адреси.

firedragon-webgl-checkbox =
    .label = Увімкнути WebGL
firedragon-webgl-description = WebGL є сильним вектором відбитків пальців. Якщо вам потрібно його увімкнути, скористайтеся розширенням на кшталт Canvas Blocker.


firedragon-security-heading = Безпека

firedragon-ocsp-checkbox =
    .label = Впровадити жорстку відмову OCSP
firedragon-ocsp-description = Заборонити підключення до веб-сайту, якщо перевірка OCSP не може бути виконана. Це підвищує безпеку, але може призвести до збоїв, якщо сервер OCSP не працює.

firedragon-goog-safe-checkbox =
    .label = Увімкніть безпечний перегляд Google
firedragon-goog-safe-description = Якщо ви турбуєтеся про шкідливе програмне забезпечення та фішинг, увімкніть його. Вимкнено з міркувань цензури, але рекомендується для менш досвідчених користувачів. Всі перевірки відбуваються локально.

firedragon-goog-safe-download-checkbox =
    .label = Сканування завантажень
firedragon-goog-safe-download-description = Дозвольте Безпечному браузеру сканувати ваші завантаження для виявлення підозрілих файлів. Всі перевірки відбуваються локально.

# Footer
firedragon-footer = Корисні посилання
firedragon-config-link =
    .label = Всі розширені налаштування (about:config)
firedragon-open-profile =
    .label = Відкрийте каталог профілю користувача
