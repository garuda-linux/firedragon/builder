## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = حول:تغييرات التكوين، مجمعة بشكل منطقي ويمكن الوصول إليها بسهولة

# Main content
firedragon-header = تفضيلات { -brand-short-name }


firedragon-general-heading = سلوك المتصفح

firedragon-extension-update-checkbox =
    .label = تحديث الوظائف الإضافية تلقائياً
firedragon-extension-update-description = حافظ على تحديث الإضافات دون تدخل يدوي. خيار جيد لأمانك. إذا كنت لا تقوم بمراجعة التعليمات البرمجية لملحقاتك قبل كل تحديث، فيجب عليك تمكين هذا الخيار.

firedragon-sync-checkbox =
    .label = تمكين مزامنة Firefox
firedragon-sync-description = مزامنة بياناتك مع المتصفحات الأخرى. يقوم Firefox Sync بتشفير البيانات محليًا قبل نقلها إلى الخادم. يتطلب إعادة التشغيل.

firedragon-autocopy-checkbox =
    .label = تمكين اللصق بنقرة في المنتصف
firedragon-autocopy-description = حدد نصًا ما لنسخه، ثم الصقه بنقرة بالوسطى على الفأرة.

firedragon-styling-checkbox =
    .label = السماح بتخصيص userChrome.css
firedragon-styling-description = قم بتمكين هذا إذا كنت ترغب في تخصيص واجهة المستخدم بقالب محمل يدويًا. تأكد من أنك تثق بموفر القالب.


firedragon-network-heading = الربط الشبكي

firedragon-ipv6-checkbox =
    .label = تمكين IPv6
firedragon-ipv6-description = السماح لـ { -brand-short-name } بالاتصال باستخدام IPv6. بدلًا من حظر IPv6 في المتصفح، نقترح تمكين ملحق خصوصية IPv6 في نظام التشغيل الخاص بك.


firedragon-privacy-heading = الخصوصية

firedragon-xorigin-ref-checkbox =
    .label = الحد من الإحالات عبر المصدر
firedragon-xorigin-ref-description = إرسال مرجع فقط على نفس الأصل. هذا يسبب انقطاعاً. بالإضافة إلى ذلك، حتى عند إرسال الإحالات المرسلة سيظل يتم قطعها.


firedragon-broken-heading = أخذ البصمات

firedragon-rfp-checkbox =
    .label = تمكين بصمة المقاومة
firedragon-rfp-description = أداة ResistFingerprinting هي أفضل أداة في فئتها لمكافحة البصمات. إذا كنت بحاجة إلى تعطيلها، فكِّر في استخدام امتداد مثل Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = تمكين علبة الرسائل
firedragon-letterboxing-description = يطبق Letterboxing هوامش حول النوافذ الخاصة بك، من أجل إرجاع مجموعة محدودة من الدقة المستديرة.

firedragon-auto-decline-canvas-checkbox =
    .label = حظر طلبات الوصول إلى اللوحة القماشية بصمت
firedragon-auto-decline-canvas-description = رفض وصول اللوحة القماشية تلقائيًا إلى مواقع الويب، دون مطالبة المستخدم. لا يزال من الممكن السماح بالوصول إلى اللوحة القماشية من شريط url.

firedragon-webgl-checkbox =
    .label = تمكين WebGL
firedragon-webgl-description = WebGL هو ناقل بصمة قوي. إذا كنت بحاجة إلى تمكينه، ففكر في استخدام امتداد مثل Canvas Blocker.


firedragon-security-heading = الأمن

firedragon-ocsp-checkbox =
    .label = فرض فشل OCSP الثابت
firedragon-ocsp-description = منع الاتصال بموقع ويب إذا تعذر إجراء فحص OCSP. يؤدي هذا إلى زيادة الأمان، ولكنه سيتسبب في حدوث عطل عند تعطل خادم OCSP.

firedragon-goog-safe-checkbox =
    .label = تمكين التصفح الآمن من Google
firedragon-goog-safe-description = إذا كنت قلقًا بشأن البرمجيات الخبيثة والتصيّد الاحتيالي، ففكر في تمكينه. تم تعطيله بسبب مخاوف الرقابة ولكن يوصى به للمستخدمين الأقل تقدماً. تتم جميع عمليات التحقق محلياً.

firedragon-goog-safe-download-checkbox =
    .label = تنزيلات المسح الضوئي
firedragon-goog-safe-download-description = اسمح للتصفح الآمن بفحص التنزيلات لتحديد الملفات المشبوهة. تتم جميع عمليات الفحص محلياً.

# Footer
firedragon-footer = روابط مفيدة
firedragon-config-link =
    .label = جميع الإعدادات المتقدمة (حول:التكوين)
firedragon-open-profile =
    .label = فتح دليل ملف تعريف المستخدم
