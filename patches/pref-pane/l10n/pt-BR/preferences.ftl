## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = alterações no about:config, agrupadas de forma lógica e facilmente acessíveis

# Main content
firedragon-header = { -brand-short-name } Preferências


firedragon-general-heading = Comportamento do navegador

firedragon-extension-update-checkbox =
    .label = Atualizar add-ons automaticamente
firedragon-extension-update-description = Mantenha as extensões atualizadas sem intervenção manual. Uma boa opção para sua segurança. Se você não revisa o código de suas extensões antes de cada atualização, deve ativar essa opção.

firedragon-sync-checkbox =
    .label = Ativar o Firefox Sync
firedragon-sync-description = Sincronize seus dados com outros navegadores. O Firefox Sync criptografa os dados localmente antes de transmiti-los ao servidor. Requer reinicialização.

firedragon-autocopy-checkbox =
    .label = Habilitar a colagem com o clique do meio
firedragon-autocopy-description = Selecione algum texto para copiá-lo e, em seguida, cole-o com um clique no meio do mouse.

firedragon-styling-checkbox =
    .label = Permitir a personalização do userChrome.css
firedragon-styling-description = Ative essa opção se quiser personalizar a interface do usuário com um tema carregado manualmente. Certifique-se de que você confia no provedor do tema.


firedragon-network-heading = Trabalho em rede

firedragon-ipv6-checkbox =
    .label = Ativar IPv6
firedragon-ipv6-description = Permitir que { -brand-short-name } se conecte usando IPv6. Em vez de bloquear o IPv6 no navegador, sugerimos ativar a extensão de privacidade do IPv6 em seu sistema operacional.


firedragon-privacy-heading = Privacidade

firedragon-xorigin-ref-checkbox =
    .label = Limite os referenciadores de origem cruzada
firedragon-xorigin-ref-description = Enviar um referenciador somente na mesma origem. Isso causa quebra. Além disso, mesmo quando enviados, os referenciadores ainda serão cortados.


firedragon-broken-heading = Impressão digital

firedragon-rfp-checkbox =
    .label = Ativar ResistFingerprinting
firedragon-rfp-description = O ResistFingerprinting é a melhor ferramenta da categoria contra impressão digital. Se você precisar desativá-la, considere o uso de uma extensão como o Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Ativar letterboxing
firedragon-letterboxing-description = O letterboxing aplica margens ao redor de suas janelas para retornar um conjunto limitado de resoluções arredondadas.

firedragon-auto-decline-canvas-checkbox =
    .label = Bloquear silenciosamente as solicitações de acesso ao canvas
firedragon-auto-decline-canvas-description = Negar automaticamente o acesso do canvas a sites, sem avisar o usuário. Ainda é possível permitir o acesso ao canvas a partir da barra de url.

firedragon-webgl-checkbox =
    .label = Ativar WebGL
firedragon-webgl-description = O WebGL é um forte vetor de impressão digital. Se você precisar ativá-lo, considere o uso de uma extensão como o Canvas Blocker.


firedragon-security-heading = Segurança

firedragon-ocsp-checkbox =
    .label = Aplicar OCSP hard-fail
firedragon-ocsp-description = Impeça a conexão a um site se a verificação OCSP não puder ser realizada. Isso aumenta a segurança, mas causará interrupções quando um servidor OCSP estiver inativo.

firedragon-goog-safe-checkbox =
    .label = Ativar a navegação segura do Google
firedragon-goog-safe-description = Se você estiver preocupado com malware e phishing, considere ativá-lo. Desativado devido a preocupações com censura, mas recomendado para usuários menos avançados. Todas as verificações ocorrem localmente.

firedragon-goog-safe-download-checkbox =
    .label = Downloads de digitalização
firedragon-goog-safe-download-description = Permita que o Safe Browsing examine seus downloads para identificar arquivos suspeitos. Todas as verificações ocorrem localmente.

# Footer
firedragon-footer = Links úteis
firedragon-config-link =
    .label = Todas as configurações avançadas (about:config)
firedragon-open-profile =
    .label = Abrir o diretório do perfil do usuário
