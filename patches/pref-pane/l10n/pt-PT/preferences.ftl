## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = alterações about:config, agrupadas de forma lógica e facilmente acessíveis

# Main content
firedragon-header = Preferências { -brand-short-name }


firedragon-general-heading = Comportamento do navegador

firedragon-extension-update-checkbox =
    .label = Atualizar add-ons automaticamente
firedragon-extension-update-description = Mantém as extensões actualizadas sem intervenção manual. Uma boa escolha para a sua segurança. Se não revê o código das suas extensões antes de cada atualização, deve ativar esta opção.

firedragon-sync-checkbox =
    .label = Ativar a sincronização do Firefox
firedragon-sync-description = Sincronize os seus dados com outros navegadores. O Firefox Sync encripta os dados localmente antes de os transmitir para o servidor. Requer reinício.

firedragon-autocopy-checkbox =
    .label = Ativar colar com o clique do meio
firedragon-autocopy-description = Seleccione algum texto para o copiar e, em seguida, cole-o com um clique do meio do rato.

firedragon-styling-checkbox =
    .label = Permitir a personalização do userChrome.css
firedragon-styling-description = Active esta opção se pretender personalizar a IU com um tema carregado manualmente. Certifique-se de que confia no fornecedor do tema.


firedragon-network-heading = Ligação em rede

firedragon-ipv6-checkbox =
    .label = Ativar IPv6
firedragon-ipv6-description = Permitir que { -brand-short-name } se ligue utilizando IPv6. Em vez de bloquear o IPv6 no browser, sugerimos que active a extensão de privacidade IPv6 no seu SO.


firedragon-privacy-heading = Privacidade

firedragon-xorigin-ref-checkbox =
    .label = Limitar os referenciadores de origem cruzada
firedragon-xorigin-ref-description = Enviar um referenciador apenas na mesma origem. Isto provoca quebras. Além disso, mesmo quando enviados, os referenciadores ainda serão cortados.


firedragon-broken-heading = Recolha de impressões digitais

firedragon-rfp-checkbox =
    .label = Ativar ResistFingerprinting
firedragon-rfp-description = O ResistFingerprinting é a melhor ferramenta anti-impressão digital da sua classe. Se precisar de a desativar, considere a utilização de uma extensão como o Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Ativar o letterboxing
firedragon-letterboxing-description = O letterboxing aplica margens à volta das janelas, de modo a devolver um conjunto limitado de resoluções arredondadas.

firedragon-auto-decline-canvas-checkbox =
    .label = Bloquear silenciosamente os pedidos de acesso ao canvas
firedragon-auto-decline-canvas-description = Negar automaticamente o acesso ao canvas a sítios Web, sem avisar o utilizador. Continua a ser possível permitir o acesso ao canvas a partir da barra de url.

firedragon-webgl-checkbox =
    .label = Ativar WebGL
firedragon-webgl-description = O WebGL é um forte vetor de impressão digital. Se precisar de o ativar, considere a utilização de uma extensão como o Canvas Blocker.


firedragon-security-heading = Segurança

firedragon-ocsp-checkbox =
    .label = Aplicar OCSP hard-fail
firedragon-ocsp-description = Impedir a ligação a um site se a verificação OCSP não puder ser efectuada. Isto aumenta a segurança, mas causará interrupções quando um servidor OCSP estiver em baixo.

firedragon-goog-safe-checkbox =
    .label = Ativar a Navegação segura do Google
firedragon-goog-safe-description = Se estiver preocupado com malware e phishing, considere activá-lo. Desativado devido a preocupações com a censura, mas recomendado para utilizadores menos avançados. Todas as verificações são efectuadas localmente.

firedragon-goog-safe-download-checkbox =
    .label = Transferências de digitalização
firedragon-goog-safe-download-description = Permitir que a Navegação Segura analise os seus downloads para identificar ficheiros suspeitos. Todas as verificações são efectuadas localmente.

# Footer
firedragon-footer = Ligações úteis
firedragon-config-link =
    .label = Todas as definições avançadas (about:config)
firedragon-open-profile =
    .label = Abrir o diretório do perfil do utilizador
