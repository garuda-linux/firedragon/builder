## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config Änderungen, logisch gruppiert und leicht zugänglich

# Main content
firedragon-header = { -brand-short-name } Einstellungen


firedragon-general-heading = Browser-Verhalten

firedragon-extension-update-checkbox =
    .label = Add-ons automatisch aktualisieren
firedragon-extension-update-description = Halten Sie Erweiterungen ohne manuellen Eingriff auf dem neuesten Stand. Eine gute Wahl für Ihre Sicherheit. Wenn Sie den Code Ihrer Erweiterungen nicht vor jeder Aktualisierung überprüfen, sollten Sie diese Option aktivieren.

firedragon-sync-checkbox =
    .label = Firefox-Synchronisation aktivieren
firedragon-sync-description = Synchronisieren Sie Ihre Daten mit anderen Browsern. Firefox Sync verschlüsselt die Daten lokal, bevor sie an den Server übertragen werden. Erfordert einen Neustart.

firedragon-autocopy-checkbox =
    .label = Einfügen mit mittlerem Klick aktivieren
firedragon-autocopy-description = Markieren Sie einen Text, um ihn zu kopieren, und fügen Sie ihn mit einem Klick der mittleren Maustaste ein.

firedragon-styling-checkbox =
    .label = Anpassung der Datei userChrome.css zulassen
firedragon-styling-description = Aktivieren Sie diese Option, wenn Sie die Benutzeroberfläche mit einem manuell geladenen Thema anpassen möchten. Stellen Sie sicher, dass Sie dem Anbieter des Themas vertrauen.


firedragon-network-heading = Netzwerk

firedragon-ipv6-checkbox =
    .label = IPv6 einschalten
firedragon-ipv6-description = Erlauben Sie { -brand-short-name }, eine Verbindung über IPv6 herzustellen. Anstatt IPv6 im Browser zu blockieren, empfehlen wir, die IPv6-Datenschutzerweiterung in Ihrem Betriebssystem zu aktivieren.


firedragon-privacy-heading = Privatsphäre

firedragon-xorigin-ref-checkbox =
    .label = Begrenzung von Cross-Origin-Referrern
firedragon-xorigin-ref-description = Senden Sie einen Referrer nur bei gleicher Herkunft. Dies führt zu Unterbrechungen. Außerdem werden Referrer auch dann noch abgeschnitten, wenn sie gesendet werden.


firedragon-broken-heading = Fingerprinting

firedragon-rfp-checkbox =
    .label = ResistFingerprinting einschalten
firedragon-rfp-description = ResistFingerprinting ist das beste Anti-Fingerprinting-Tool seiner Klasse. Wenn Sie es deaktivieren müssen, sollten Sie eine Erweiterung wie Canvas Blocker verwenden.

firedragon-letterboxing-checkbox =
    .label = Letterboxing aktivieren
firedragon-letterboxing-description = Beim Letterboxing werden Ränder um die Fenster gelegt, um eine begrenzte Anzahl von abgerundeten Auflösungen zu erhalten.

firedragon-auto-decline-canvas-checkbox =
    .label = Canvas-Zugriffsanfragen stillschweigend blockieren
firedragon-auto-decline-canvas-description = Automatisches Verweigern des Canvas-Zugriffs auf Websites, ohne dass der Benutzer dazu aufgefordert wird. Es ist weiterhin möglich, den Canvas-Zugriff über die URL-Leiste zuzulassen.

firedragon-webgl-checkbox =
    .label = WebGL aktivieren
firedragon-webgl-description = WebGL ist ein starker Fingerprinting-Vektor. Wenn Sie es aktivieren müssen, sollten Sie eine Erweiterung wie Canvas Blocker verwenden.


firedragon-security-heading = Sicherheit

firedragon-ocsp-checkbox =
    .label = OCSP-Hardfail erzwingen
firedragon-ocsp-description = Verhindern Sie die Verbindung zu einer Website, wenn die OCSP-Prüfung nicht durchgeführt werden kann. Dies erhöht die Sicherheit, führt aber zu Unterbrechungen, wenn ein OCSP-Server ausfällt.

firedragon-goog-safe-checkbox =
    .label = Google Safe Browsing aktivieren
firedragon-goog-safe-description = Wenn Sie sich Sorgen über Malware und Phishing machen, sollten Sie diese Funktion aktivieren. Deaktiviert wegen Zensurbedenken, aber empfohlen für weniger fortgeschrittene Benutzer. Alle Überprüfungen finden lokal statt.

firedragon-goog-safe-download-checkbox =
    .label = Downloads scannen
firedragon-goog-safe-download-description = Erlauben Sie Safe Browsing, Ihre Downloads zu scannen, um verdächtige Dateien zu identifizieren. Alle Prüfungen erfolgen lokal.

# Footer
firedragon-footer = Nützliche Links
firedragon-config-link =
    .label = Alle erweiterten Einstellungen (about:config)
firedragon-open-profile =
    .label = Benutzerprofilverzeichnis öffnen
