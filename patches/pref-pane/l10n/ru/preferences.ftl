## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = изменения в about:config, логически сгруппированные и легко доступные

# Main content
firedragon-header = { -brand-short-name } Предпочтения


firedragon-general-heading = Поведение браузера

firedragon-extension-update-checkbox =
    .label = Автоматическое обновление дополнений
firedragon-extension-update-description = Поддерживайте расширения в актуальном состоянии без ручного вмешательства. Хороший выбор для вашей безопасности. Если вы не проверяете код своих расширений перед каждым обновлением, включите эту опцию.

firedragon-sync-checkbox =
    .label = Включить синхронизацию Firefox
firedragon-sync-description = Синхронизируйте свои данные с другими браузерами. Firefox Sync шифрует данные локально перед передачей на сервер. Требуется перезагрузка.

firedragon-autocopy-checkbox =
    .label = Включить вставку по среднему щелчку мыши
firedragon-autocopy-description = Выделите текст, чтобы скопировать его, а затем вставьте его с помощью среднего щелчка мыши.

firedragon-styling-checkbox =
    .label = Разрешите настройку userChrome.css
firedragon-styling-description = Включите эту опцию, если вы хотите настроить пользовательский интерфейс с помощью загруженной вручную темы. Убедитесь, что вы доверяете поставщику темы.


firedragon-network-heading = Работа в сети

firedragon-ipv6-checkbox =
    .label = Включить IPv6
firedragon-ipv6-description = Разрешите { -brand-short-name } подключаться с помощью IPv6. Вместо того чтобы блокировать IPv6 в браузере, мы предлагаем включить расширение IPv6 privacy в вашей ОС.


firedragon-privacy-heading = Конфиденциальность

firedragon-xorigin-ref-checkbox =
    .label = Ограничьте кросс-оригинальных рефералов
firedragon-xorigin-ref-description = Отправляйте реферер только на same-origin. Это приводит к поломке. Кроме того, даже при отправке рефереры все равно будут обрезаны.


firedragon-broken-heading = Снятие отпечатков пальцев

firedragon-rfp-checkbox =
    .label = Включить функцию ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting - лучший в своем классе инструмент для борьбы с отпечатками пальцев. Если вам нужно отключить его, воспользуйтесь расширением вроде Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Включить letterboxing
firedragon-letterboxing-description = Letterboxing накладывает поля вокруг окон, чтобы вернуть ограниченный набор округлых разрешений.

firedragon-auto-decline-canvas-checkbox =
    .label = Бесшумно блокируйте запросы на доступ к холсту
firedragon-auto-decline-canvas-description = Автоматически запрещайте доступ к веб-сайтам с помощью canvas, не спрашивая пользователя. По-прежнему можно разрешить доступ к холсту из urlbar.

firedragon-webgl-checkbox =
    .label = Включите WebGL
firedragon-webgl-description = WebGL - это сильный вектор отпечатков пальцев. Если вам нужно его включить, воспользуйтесь расширением вроде Canvas Blocker.


firedragon-security-heading = Безопасность

firedragon-ocsp-checkbox =
    .label = Обеспечение жесткого отказа OCSP
firedragon-ocsp-description = Запретите подключение к веб-сайту, если проверка OCSP не может быть выполнена. Это повышает безопасность, но приводит к сбоям, когда сервер OCSP не работает.

firedragon-goog-safe-checkbox =
    .label = Включите безопасный просмотр Google
firedragon-goog-safe-description = Если вы беспокоитесь о вредоносном ПО и фишинге, подумайте о его включении. Отключено по соображениям цензуры, но рекомендуется для менее продвинутых пользователей. Все проверки происходят локально.

firedragon-goog-safe-download-checkbox =
    .label = Сканирование загрузок
firedragon-goog-safe-download-description = Разрешите Safe Browsing сканировать загружаемые файлы, чтобы выявить подозрительные. Все проверки происходят локально.

# Footer
firedragon-footer = Полезные ссылки
firedragon-config-link =
    .label = Все дополнительные настройки (about:config)
firedragon-open-profile =
    .label = Откройте каталог профиля пользователя
