## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config-ændringer, logisk grupperet og let tilgængelige

# Main content
firedragon-header = { -brand-short-name } Indstillinger


firedragon-general-heading = Browser-adfærd

firedragon-extension-update-checkbox =
    .label = Opdater add-ons automatisk
firedragon-extension-update-description = Hold udvidelser opdateret uden manuel indgriben. Et godt valg for din sikkerhed. Hvis du ikke gennemgår koden til dine udvidelser før hver opdatering, bør du aktivere denne mulighed.

firedragon-sync-checkbox =
    .label = Aktivér Firefox-synkronisering
firedragon-sync-description = Synkroniser dine data med andre browsere. Firefox Sync krypterer data lokalt, før de sendes til serveren. Kræver genstart.

firedragon-autocopy-checkbox =
    .label = Aktivér indsætning med mellemklik
firedragon-autocopy-description = Vælg noget tekst for at kopiere den, og indsæt den derefter med et klik med den midterste mus.

firedragon-styling-checkbox =
    .label = Tillad tilpasning af userChrome.css
firedragon-styling-description = Aktivér dette, hvis du vil tilpasse brugergrænsefladen med et manuelt indlæst tema. Sørg for, at du har tillid til udbyderen af temaet.


firedragon-network-heading = Netværk

firedragon-ipv6-checkbox =
    .label = Aktivér IPv6
firedragon-ipv6-description = Tillad { -brand-short-name } at oprette forbindelse ved hjælp af IPv6. I stedet for at blokere IPv6 i browseren foreslår vi, at du aktiverer IPv6-privatlivsudvidelsen i dit operativsystem.


firedragon-privacy-heading = Privatlivets fred

firedragon-xorigin-ref-checkbox =
    .label = Begræns henvisninger på tværs af oprindelse
firedragon-xorigin-ref-description = Send kun en referrer ved samme oprindelse. Dette forårsager brud. Derudover vil henvisninger stadig blive trimmet, selv når de er sendt.


firedragon-broken-heading = Fingeraftryk

firedragon-rfp-checkbox =
    .label = Aktivér ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting er det bedste anti-fingeraftryksværktøj i sin klasse. Hvis du har brug for at slå det fra, kan du overveje at bruge en udvidelse som Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Aktivér letterboxing
firedragon-letterboxing-description = Letterboxing anvender margener omkring dine vinduer for at returnere et begrænset sæt afrundede opløsninger.

firedragon-auto-decline-canvas-checkbox =
    .label = Bloker stille anmodninger om adgang til lærred
firedragon-auto-decline-canvas-description = Nægt automatisk canvas-adgang til hjemmesider uden at spørge brugeren. Det er stadig muligt at tillade canvas-adgang fra url-baren.

firedragon-webgl-checkbox =
    .label = Aktivér WebGL
firedragon-webgl-description = WebGL er en stærk fingeraftryksvektor. Hvis du har brug for at aktivere det, kan du overveje at bruge en udvidelse som Canvas Blocker.


firedragon-security-heading = Sikkerhed

firedragon-ocsp-checkbox =
    .label = Gennemfør OCSP hard-fail
firedragon-ocsp-description = Undgå at oprette forbindelse til et websted, hvis OCSP-tjekket ikke kan udføres. Dette øger sikkerheden, men det vil medføre afbrydelser, når en OCSP-server er nede.

firedragon-goog-safe-checkbox =
    .label = Aktivér Google Safe Browsing
firedragon-goog-safe-description = Hvis du er bekymret for malware og phishing, bør du overveje at aktivere det. Deaktiveret af hensyn til censur, men anbefales til mindre avancerede brugere. Alle kontroller sker lokalt.

firedragon-goog-safe-download-checkbox =
    .label = Scanning af downloads
firedragon-goog-safe-download-description = Lad Safe Browsing scanne dine downloads for at identificere mistænkelige filer. Alle kontroller sker lokalt.

# Footer
firedragon-footer = Nyttige links
firedragon-config-link =
    .label = Alle avancerede indstillinger (about:config)
firedragon-open-profile =
    .label = Åbn mappen med brugerprofiler
