## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config wijzigingen, logisch gegroepeerd en gemakkelijk toegankelijk

# Main content
firedragon-header = { -brand-short-name } Voorkeuren


firedragon-general-heading = Browser gedrag

firedragon-extension-update-checkbox =
    .label = Add-ons automatisch bijwerken
firedragon-extension-update-description = Houd extensies up-to-date zonder handmatige tussenkomst. Een goede keuze voor je veiligheid. Als je de code van je extensies niet voor elke update controleert, moet je deze optie inschakelen.

firedragon-sync-checkbox =
    .label = Firefox-synchronisatie inschakelen
firedragon-sync-description = Synchroniseer uw gegevens met andere browsers. Firefox Sync codeert gegevens lokaal voordat deze naar de server worden verzonden. Vereist opnieuw opstarten.

firedragon-autocopy-checkbox =
    .label = Plakken met middelste klik inschakelen
firedragon-autocopy-description = Selecteer wat tekst om het te kopiëren en plak het dan met een klik met de middelste muisknop.

firedragon-styling-checkbox =
    .label = Aanpassing userChrome.css toestaan
firedragon-styling-description = Schakel dit in als je de UI wilt aanpassen met een handmatig geladen thema. Zorg ervoor dat je de aanbieder van het thema vertrouwt.


firedragon-network-heading = Netwerken

firedragon-ipv6-checkbox =
    .label = IPv6 inschakelen
firedragon-ipv6-description = Sta { -brand-short-name } toe om verbinding te maken via IPv6. In plaats van IPv6 te blokkeren in de browser, raden we aan om de IPv6-privacy extensie in te schakelen in uw besturingssysteem.


firedragon-privacy-heading = Privacy

firedragon-xorigin-ref-checkbox =
    .label = Beperk cross-origin verwijzers
firedragon-xorigin-ref-description = Stuur een verwijzer alleen op dezelfde oorsprong. Dit veroorzaakt breuk. Bovendien worden referrers nog steeds bijgesneden, zelfs als ze worden verzonden.


firedragon-broken-heading = Vingerafdrukken

firedragon-rfp-checkbox =
    .label = ResistFingerprinting inschakelen
firedragon-rfp-description = ResistFingerprinting is het beste anti-vingerafdrukprogramma in zijn klasse. Als je het wilt uitschakelen, overweeg dan het gebruik van een extensie zoals Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Letterboxing inschakelen
firedragon-letterboxing-description = Letterboxing past marges toe rond je vensters, om een beperkte set afgeronde resoluties terug te geven.

firedragon-auto-decline-canvas-checkbox =
    .label = Stil toegangsverzoeken voor canvas blokkeren
firedragon-auto-decline-canvas-description = Weiger automatisch de toegang tot canvas op websites, zonder de gebruiker te vragen. Het is nog steeds mogelijk om canvas toegang toe te staan vanuit de urlbar.

firedragon-webgl-checkbox =
    .label = WebGL inschakelen
firedragon-webgl-description = WebGL is een sterke vector voor vingerafdrukken. Als je het moet inschakelen, overweeg dan het gebruik van een extensie zoals Canvas Blocker.


firedragon-security-heading = Beveiliging

firedragon-ocsp-checkbox =
    .label = OCSP hard-fail afdwingen
firedragon-ocsp-description = Voorkomen dat er verbinding wordt gemaakt met een website als de OCSP controle niet kan worden uitgevoerd. Dit verhoogt de veiligheid, maar veroorzaakt een storing als een OCSP server down is.

firedragon-goog-safe-checkbox =
    .label = Google Safe Browsing inschakelen
firedragon-goog-safe-description = Als je je zorgen maakt over malware en phishing, overweeg dan om het in te schakelen. Uitgeschakeld vanwege censuur, maar aanbevolen voor minder geavanceerde gebruikers. Alle controles gebeuren lokaal.

firedragon-goog-safe-download-checkbox =
    .label = Downloads scannen
firedragon-goog-safe-download-description = Laat Safe Browsing je downloads scannen om verdachte bestanden te identificeren. Alle controles gebeuren lokaal.

# Footer
firedragon-footer = Nuttige links
firedragon-config-link =
    .label = Alle geavanceerde instellingen (about:config)
firedragon-open-profile =
    .label = Open de map met gebruikersprofielen
