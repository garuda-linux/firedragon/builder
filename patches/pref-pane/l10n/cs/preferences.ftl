## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = logicky seskupené a snadno přístupné změny v about:config

# Main content
firedragon-header = Předvolby { -brand-short-name }


firedragon-general-heading = Chování prohlížeče

firedragon-extension-update-checkbox =
    .label = Automatická aktualizace doplňků
firedragon-extension-update-description = Aktualizujte rozšíření bez nutnosti ručního zásahu. Dobrá volba pro vaši bezpečnost. Pokud nekontrolujete kód svých rozšíření před každou aktualizací, měli byste tuto možnost povolit.

firedragon-sync-checkbox =
    .label = Povolení synchronizace Firefoxu
firedragon-sync-description = Synchronizace dat s ostatními prohlížeči. Firefox Sync šifruje data lokálně před jejich přenosem na server. Vyžaduje restart.

firedragon-autocopy-checkbox =
    .label = Povolit vložení prostředním kliknutím
firedragon-autocopy-description = Vyberte nějaký text, abyste jej zkopírovali, a pak jej vložte kliknutím prostředním tlačítkem myši.

firedragon-styling-checkbox =
    .label = Povolení přizpůsobení souboru userChrome.css
firedragon-styling-description = Tuto možnost povolte, pokud chcete přizpůsobit uživatelské rozhraní pomocí ručně načteného motivu. Ujistěte se, že důvěřujete poskytovateli motivu.


firedragon-network-heading = Vytváření sítí

firedragon-ipv6-checkbox =
    .label = Povolení protokolu IPv6
firedragon-ipv6-description = Povolit { -brand-short-name } připojení pomocí protokolu IPv6. Místo blokování protokolu IPv6 v prohlížeči doporučujeme povolit v operačním systému rozšíření pro ochranu soukromí IPv6.


firedragon-privacy-heading = Ochrana osobních údajů

firedragon-xorigin-ref-checkbox =
    .label = Omezení křížových odkazů
firedragon-xorigin-ref-description = Odesílání odkazu pouze na stejný původ. To způsobuje rozbití. Navíc i po odeslání budou referrery ořezány.


firedragon-broken-heading = snímání otisků prstů

firedragon-rfp-checkbox =
    .label = Povolit funkci ResistFingerprinting
firedragon-rfp-description = ResistFingerprinting je nejlepší nástroj proti otiskům prstů ve své třídě. Pokud jej potřebujete zakázat, zvažte použití rozšíření, jako je Canvas Blocker.

firedragon-letterboxing-checkbox =
    .label = Povolit funkci letterboxing
firedragon-letterboxing-description = Letterboxing používá okraje kolem oken, aby se vrátila omezená sada zaoblených rozlišení.

firedragon-auto-decline-canvas-checkbox =
    .label = Tiché blokování požadavků na přístup k plátnu
firedragon-auto-decline-canvas-description = Automatické odepření přístupu k webovým stránkám na plátně bez vyzvání uživatele. Stále je možné povolit přístup k plátnu z panelu url.

firedragon-webgl-checkbox =
    .label = Povolení WebGL
firedragon-webgl-description = WebGL je silným vektorem otisků prstů. Pokud jej potřebujete povolit, zvažte použití rozšíření, jako je Canvas Blocker.


firedragon-security-heading = Zabezpečení

firedragon-ocsp-checkbox =
    .label = Vynucení protokolu OCSP při selhání
firedragon-ocsp-description = Zabránit připojení k webové stránce, pokud nelze provést kontrolu OCSP. To zvyšuje bezpečnost, ale způsobí to poruchu, když je server OCSP mimo provoz.

firedragon-goog-safe-checkbox =
    .label = Povolení funkce Bezpečné prohlížení Google
firedragon-goog-safe-description = Pokud se obáváte malwaru a phishingu, zvažte jeho povolení. Zakázáno kvůli obavám z cenzury, ale doporučeno pro méně pokročilé uživatele. Všechny kontroly probíhají lokálně.

firedragon-goog-safe-download-checkbox =
    .label = Stažené soubory ke skenování
firedragon-goog-safe-download-description = Povolte bezpečnému procházení skenování stažených souborů za účelem identifikace podezřelých souborů. Všechny kontroly probíhají lokálně.

# Footer
firedragon-footer = Užitečné odkazy
firedragon-config-link =
    .label = Všechna pokročilá nastavení (about:config)
firedragon-open-profile =
    .label = Otevření adresáře uživatelského profilu
