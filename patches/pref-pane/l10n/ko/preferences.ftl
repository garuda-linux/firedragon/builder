## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config 변경 사항을 논리적으로 그룹화하여 쉽게 액세스할 수 있습니다.

# Main content
firedragon-header = { -brand-short-name } 기본 설정


firedragon-general-heading = 브라우저 동작

firedragon-extension-update-checkbox =
    .label = 애드온 자동 업데이트
firedragon-extension-update-description = 수동 개입 없이 확장 프로그램을 최신 상태로 유지하세요. 보안을 위한 좋은 선택입니다. 매번 업데이트하기 전에 확장 프로그램의 코드를 검토하지 않는다면 이 옵션을 사용 설정해야 합니다.

firedragon-sync-checkbox =
    .label = Firefox 동기화 활성화
firedragon-sync-description = 다른 브라우저와 데이터를 동기화하세요. Firefox 동기화는 데이터를 서버로 전송하기 전에 로컬에서 암호화합니다. 다시 시작해야 합니다.

firedragon-autocopy-checkbox =
    .label = 가운데 클릭 붙여넣기 활성화
firedragon-autocopy-description = 복사할 텍스트를 선택한 다음 마우스 가운데 버튼을 클릭하여 붙여넣습니다.

firedragon-styling-checkbox =
    .label = userChrome.css 사용자 정의 허용
firedragon-styling-description = 수동으로 로드한 테마로 UI를 사용자 지정하려면 이 옵션을 활성화합니다. 테마 제공업체를 신뢰할 수 있는지 확인하세요.


firedragon-network-heading = 네트워킹

firedragon-ipv6-checkbox =
    .label = IPv6 사용
firedragon-ipv6-description = { -brand-short-name }가 IPv6를 사용하여 연결하도록 허용합니다. 브라우저에서 IPv6를 차단하는 대신 OS에서 IPv6 개인정보 보호 확장을 활성화하는 것이 좋습니다.


firedragon-privacy-heading = 개인 정보 보호

firedragon-xorigin-ref-checkbox =
    .label = 교차 출처 리퍼러 제한하기
firedragon-xorigin-ref-description = 출처가 동일한 경우에만 리퍼러를 보내세요. 이로 인해 파손이 발생합니다. 또한 전송된 리퍼러도 여전히 트리밍됩니다.


firedragon-broken-heading = 지문 인식

firedragon-rfp-checkbox =
    .label = 레지스트 핑거프린팅 활성화
firedragon-rfp-description = ResistFingerprinting은 동급 최고의 지문 인식 방지 도구입니다. 비활성화해야 하는 경우 캔버스 블로커와 같은 확장 프로그램을 사용하세요.

firedragon-letterboxing-checkbox =
    .label = 레터박스 사용
firedragon-letterboxing-description = 레터박싱은 창 주위에 여백을 적용하여 제한된 둥근 해상도 세트를 반환합니다.

firedragon-auto-decline-canvas-checkbox =
    .label = 캔버스 액세스 요청을 자동으로 차단
firedragon-auto-decline-canvas-description = 사용자에게 메시지를 표시하지 않고 웹사이트에 대한 캔버스 액세스를 자동으로 거부합니다. URL 표시줄에서 캔버스 액세스를 허용하는 것은 여전히 가능합니다.

firedragon-webgl-checkbox =
    .label = WebGL 사용
firedragon-webgl-description = WebGL은 강력한 핑거프린팅 벡터입니다. 이를 활성화해야 하는 경우 캔버스 블로커와 같은 확장 프로그램을 사용하는 것이 좋습니다.


firedragon-security-heading = 보안

firedragon-ocsp-checkbox =
    .label = OCSP 하드 페일 적용
firedragon-ocsp-description = OCSP 검사를 수행할 수 없는 경우 웹사이트에 연결하지 않도록 설정합니다. 이렇게 하면 보안이 강화되지만 OCSP 서버가 다운될 경우 중단될 수 있습니다.

firedragon-goog-safe-checkbox =
    .label = Google 세이프 브라우징 사용
firedragon-goog-safe-description = 멀웨어 및 피싱이 걱정된다면 이 기능을 사용하도록 설정하세요. 검열 우려로 인해 비활성화되어 있지만 고급 사용자에게 권장됩니다. 모든 검사는 로컬에서 이루어집니다.

firedragon-goog-safe-download-checkbox =
    .label = 다운로드 스캔
firedragon-goog-safe-download-description = 세이프 브라우징이 다운로드를 검사하여 의심스러운 파일을 식별하도록 허용합니다. 모든 검사는 로컬에서 이루어집니다.

# Footer
firedragon-footer = 유용한 링크
firedragon-config-link =
    .label = 모든 고급 설정(about:config)
firedragon-open-profile =
    .label = 사용자 프로필 디렉터리 열기
