## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:config değişiklikleri, mantıksal olarak gruplandırılmış ve kolayca erişilebilir

# Main content
firedragon-header = { -brand-short-name } Tercihler


firedragon-general-heading = Tarayıcı Davranışı

firedragon-extension-update-checkbox =
    .label = Eklentileri otomatik olarak güncelleyin
firedragon-extension-update-description = Manuel müdahale olmadan uzantıları güncel tutun. Güvenliğiniz için iyi bir seçim. Her güncellemeden önce uzantılarınızın kodunu gözden geçirmiyorsanız, bu seçeneği etkinleştirmelisiniz.

firedragon-sync-checkbox =
    .label = Firefox Sync'i Etkinleştir
firedragon-sync-description = Verilerinizi diğer tarayıcılarla senkronize edin. Firefox Sync, verileri sunucuya iletmeden önce yerel olarak şifreler. Yeniden başlatma gerektirir.

firedragon-autocopy-checkbox =
    .label = Orta tıklama yapıştırmasını etkinleştir
firedragon-autocopy-description = Kopyalamak için bir metin seçin, ardından orta fare tıklamasıyla yapıştırın.

firedragon-styling-checkbox =
    .label = userChrome.css özelleştirmesine izin ver
firedragon-styling-description = Kullanıcı arayüzünü manuel olarak yüklenen bir tema ile özelleştirmek istiyorsanız bunu etkinleştirin. Temanın sağlayıcısına güvendiğinizden emin olun.


firedragon-network-heading = Ağ İletişimi

firedragon-ipv6-checkbox =
    .label = IPv6'yı Etkinleştir
firedragon-ipv6-description = IPv6 kullanarak bağlanmak için { -brand-short-name } öğesine izin verin. Tarayıcıda IPv6'yı engellemek yerine, işletim sisteminizde IPv6 gizlilik uzantısını etkinleştirmenizi öneririz.


firedragon-privacy-heading = Gizlilik

firedragon-xorigin-ref-checkbox =
    .label = Çapraz kaynaklı yönlendiricileri sınırlayın
firedragon-xorigin-ref-description = Yalnızca aynı kaynaktan yönlendirici gönderin. Bu kırılmaya neden olur. Ek olarak, yönlendiriciler gönderilse bile yine de kırpılacaktır.


firedragon-broken-heading = Parmak İzi

firedragon-rfp-checkbox =
    .label = ResistFingerprinting'i Etkinleştir
firedragon-rfp-description = ResistFingerprinting sınıfının en iyi parmak izi önleme aracıdır. Devre dışı bırakmanız gerekiyorsa, Canvas Blocker gibi bir uzantı kullanmayı düşünün.

firedragon-letterboxing-checkbox =
    .label = Mektup kutusunu etkinleştir
firedragon-letterboxing-description = Letterboxing, sınırlı bir yuvarlatılmış çözünürlük kümesi döndürmek için pencerelerinizin etrafına kenar boşlukları uygular.

firedragon-auto-decline-canvas-checkbox =
    .label = Tuval erişim isteklerini sessizce engelleyin
firedragon-auto-decline-canvas-description = Kullanıcıya sormadan web sitelerine canvas erişimini otomatik olarak reddedin. Tuval erişimine urlbar'dan izin vermek hala mümkündür.

firedragon-webgl-checkbox =
    .label = WebGL'yi Etkinleştir
firedragon-webgl-description = WebGL güçlü bir parmak izi vektörüdür. Etkinleştirmeniz gerekiyorsa, Canvas Blocker gibi bir uzantı kullanmayı düşünün.


firedragon-security-heading = Güvenlik

firedragon-ocsp-checkbox =
    .label = OCSP zor başarısızlığını zorla
firedragon-ocsp-description = OCSP denetimi gerçekleştirilemiyorsa bir web sitesine bağlanmayı önleyin. Bu güvenliği artırır, ancak bir OCSP sunucusu kapalı olduğunda kırılmaya neden olur.

firedragon-goog-safe-checkbox =
    .label = Google Güvenli Tarama'yı etkinleştirin
firedragon-goog-safe-description = Kötü amaçlı yazılım ve kimlik avı konusunda endişeleriniz varsa etkinleştirmeyi düşünün. Sansür endişeleri nedeniyle devre dışı bırakıldı ancak daha az gelişmiş kullanıcılar için önerilir. Tüm kontroller yerel olarak gerçekleşir.

firedragon-goog-safe-download-checkbox =
    .label = İndirmeleri tara
firedragon-goog-safe-download-description = Güvenli Tarama'nın şüpheli dosyaları tanımlamak için indirmelerinizi taramasına izin verin. Tüm kontroller yerel olarak gerçekleşir.

# Footer
firedragon-footer = Faydalı bağlantılar
firedragon-config-link =
    .label = Tüm gelişmiş ayarlar (about:config)
firedragon-open-profile =
    .label = Kullanıcı profili dizinini açın
