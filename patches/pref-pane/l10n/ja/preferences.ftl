## FireDragon preferences

# Sidebar
pane-firedragon-title = { -brand-short-name }
category-firedragon =
    .tooltiptext = about:configの変更を論理的にグループ化し、簡単にアクセスできるようにした。

# Main content
firedragon-header = { -brand-short-name }の好み


firedragon-general-heading = ブラウザの動作

firedragon-extension-update-checkbox =
    .label = アドオンの自動更新
firedragon-extension-update-description = 手動で操作することなく、拡張機能を常に最新の状態に保ちます。セキュリティのために良い選択です。エクステンションのコードを毎回更新前に確認しないのであれば、このオプションを有効にすべきです。

firedragon-sync-checkbox =
    .label = Firefoxの同期を有効にする
firedragon-sync-description = 他のブラウザとデータを同期。Firefox Sync は、データをサーバに送信する前にローカルで暗号化します。再起動が必要です。

firedragon-autocopy-checkbox =
    .label = 中クリックでのペーストを有効にする
firedragon-autocopy-description = テキストを選択してコピーし、マウスを中クリックして貼り付ける。

firedragon-styling-checkbox =
    .label = userChrome.cssのカスタマイズを許可する
firedragon-styling-description = 手動でロードしたテーマでUIをカスタマイズしたい場合は、これを有効にしてください。テーマの提供者が信頼できることを確認してください。


firedragon-network-heading = ネットワーキング

firedragon-ipv6-checkbox =
    .label = IPv6を有効にする
firedragon-ipv6-description = { -brand-short-name }にIPv6での接続を許可する。ブラウザでIPv6をブロックする代わりに、OSのIPv6プライバシー拡張機能を有効にすることをお勧めします。


firedragon-privacy-heading = プライバシー

firedragon-xorigin-ref-checkbox =
    .label = クロスオリジン・リファラーを制限する
firedragon-xorigin-ref-description = 同一オリジンでのみリファラを送信する。これは破損の原因になります。さらに、リファラーを送信してもトリミングされます。


firedragon-broken-heading = 指紋押捺

firedragon-rfp-checkbox =
    .label = レジストフィンガープリントを有効にする
firedragon-rfp-description = ResistFingerprintingはクラス最高のアンチフィンガープリントツールです。無効にする必要がある場合は、Canvas Blockerのような拡張機能の使用を検討してください。

firedragon-letterboxing-checkbox =
    .label = レターボックスを有効にする
firedragon-letterboxing-description = レターボックスは、限られた丸みを帯びた解像度を返すために、ウィンドウの周囲に余白を適用します。

firedragon-auto-decline-canvas-checkbox =
    .label = キャンバスのアクセス要求をサイレントブロック
firedragon-auto-decline-canvas-description = ユーザーにプロンプトを表示することなく、Web サイトへの canvas アクセスを自動的に拒否します。urlbar からキャンバスへのアクセスを許可することは可能です。

firedragon-webgl-checkbox =
    .label = WebGLを有効にする
firedragon-webgl-description = WebGLは強力なフィンガープリントベクターです。もし有効にする必要があるなら、Canvas Blockerのような拡張機能の使用を検討してください。


firedragon-security-heading = セキュリティ

firedragon-ocsp-checkbox =
    .label = OCSPハードフェイルの強制
firedragon-ocsp-description = OCSPチェックが実行できない場合、ウェブサイトへの接続を禁止する。これにより、セキュリティは向上するが、OCSPサーバーがダウンした場合に障害が発生する。

firedragon-goog-safe-checkbox =
    .label = Googleセーフブラウジングを有効にする
firedragon-goog-safe-description = マルウェアやフィッシングが心配な方は、有効にしておくことをお勧めします。検閲の懸念があるため無効にしていますが、あまり高度でないユーザーにはお勧めします。すべてのチェックはローカルで行われます。

firedragon-goog-safe-download-checkbox =
    .label = スキャンのダウンロード
firedragon-goog-safe-download-description = セーフブラウジングがダウンロードしたファイルをスキャンし、不審なファイルを特定できるようにします。すべてのチェックはローカルで行われます。

# Footer
firedragon-footer = お役立ちリンク
firedragon-config-link =
    .label = すべての詳細設定（about:config）
firedragon-open-profile =
    .label = ユーザープロファイルディレクトリを開く
