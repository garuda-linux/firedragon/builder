clean-downloads: clean.downloads.txt
	@echo clean.downloads.txt | cat clean.downloads.txt - | sort -u | xargs -rtn1 rm -rf

clean-folders: clean.folders.txt
	@echo clean.folders.txt | cat clean.folders.txt - | sort -u | xargs -rtn1 rm -rf

clean-targets: clean.targets.txt
	@echo clean.targets.txt | cat clean.targets.txt - | sort -u | xargs -rtn1 rm -rf

.PHONY: clean-downloads clean-folders clean-targets
