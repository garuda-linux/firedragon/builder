source_basename := $(firedragon_catppuccin_basename).source.tar.zst
target_basename := $(firedragon_catppuccin_basename).linux-x86_64

all: package

$(source_basename):
	@$(MAKE) -f $(srcdir)/assets/source-catppuccin.mk

$(target_basename): $(source_basename)
	@echo $@ >> clean.folders.txt
	@rm -rf $@
	@mkdir $@
	tar --strip-components=1 $(UNCOMPRESS_ARGS) -xf $< -C $@

$(target_basename)/mozconfig: $(target_basename) $(srcdir)/assets/mozconfig $(srcdir)/assets/mozconfig.linux-x86_64
	cat $(srcdir)/assets/mozconfig $(srcdir)/assets/mozconfig.linux-x86_64 > $(target_basename)/mozconfig
	sed -i 's#browser/branding/firedragon#browser/branding/firedragon-catppuccin#' $(target_basename)/mozconfig

build: $(target_basename)/mozconfig
	./$(target_basename)/mach build

package: build
	./$(target_basename)/mach package
	./$(target_basename)/mach package-multi-locale --locales $(FIREDRAGON_LOCALES)
	@echo $(target_basename).tar.zst >> clean.targets.txt
	tar $(COMPRESS_ARGS) -cf $(target_basename).tar.zst --exclude pingsender -C $(target_basename)/obj/dist firedragon

.PHONY: all build package
