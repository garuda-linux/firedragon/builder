all: package

$(firedragon_basename).source.tar.zst:
	@$(MAKE) -f $(srcdir)/assets/source.mk

$(firedragon_catppuccin_basename): $(firedragon_basename).source.tar.zst
	@echo $@ >> clean.folders.txt
	@rm -rf $@
	@mkdir $@
	tar --strip-components=1 $(UNCOMPRESS_ARGS) -xf $< -C $@

build: $(firedragon_catppuccin_basename)
	sed -i 's#/sweet-dark/#/catppuccin-mocha-mauve/#' "$(firedragon_catppuccin_basename)/firedragon/distribution/policies.json"

package: build
	@echo $(firedragon_catppuccin_basename).tar.zst >> clean.targets.txt
	tar $(COMPRESS_ARGS) -cf $(firedragon_catppuccin_basename).source.tar.zst $(firedragon_catppuccin_basename)

.PHONY: all build package
