export destdir ?= $(abspath $(DESTDIR))

all: $(dirs) $(patches)

$(dirs): %: %/GNUmakefile
	@$(MAKE) -C $*

$(patches): %: %.patch
	patch -Nsp1 -d $(destdir) -i $(srcdir)/$*.patch

.PHONY: all $(dirs) $(patches)
