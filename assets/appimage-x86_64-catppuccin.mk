source_basename := $(firedragon_catppuccin_basename).linux-x86_64.tar.zst
target_basename := $(firedragon_catppuccin_basename).appimage-x86_64

all: build

$(source_basename):
	@$(MAKE) -f $(srcdir)/assets/linux-x86_64.mk package

$(target_basename):
	@echo $@ >> clean.folders.txt
	@mkdir $@

$(target_basename)/AppDir: $(target_basename) $(source_basename)
	@rm -rf $@
	@mkdir $@
	tar --strip-components=1 $(UNCOMPRESS_ARGS) -xf $(source_basename) -C $@
	sed 's#/usr/lib/firedragon/firedragon#firedragon#' $(srcdir)/settings/firedragon.desktop > $@/firedragon.desktop
	cp $@/browser/chrome/icons/default/default128.png $@/firedragon.png
	cp $(srcdir)/assets/appimage/AppRun $@/AppRun
	chmod +x $@/AppRun

$(target_basename)/appimagetool-x86_64.AppImage:
	curl -fLo $@ 'https://github.com/AppImage/appimagetool/releases/download/continuous/appimagetool-x86_64.AppImage'
	chmod a+x $@

build: $(target_basename)/AppDir $(target_basename)/appimagetool-x86_64.AppImage
	$(target_basename)/appimagetool-x86_64.AppImage $(target_basename)/AppDir $(target_basename).AppImage

.PHONY: all build
