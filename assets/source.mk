all: package

$(floorp_basename):
	@echo $(floorp_basename) >> clean.folders.txt
	git clone -b v$(FLOORP_VERSION) -c advice.detachedHead=false --depth 1 https://github.com/Floorp-Projects/Floorp.git $(floorp_basename)
	git -C $(floorp_basename) submodule update --init --recursive

copy: $(floorp_basename)
	@echo $(firedragon_basename) >> clean.folders.txt
	rsync -a --delete --exclude=.git $(floorp_basename)/ $(firedragon_basename)/

build: copy
	cp -a $(srcdir)/source_files/. $(firedragon_basename)
	@$(MAKE) -C $(srcdir)/patches DESTDIR=$(real_build_dir)/$(firedragon_basename)
	@$(MAKE) -C $(srcdir)/settings DESTDIR=$(real_build_dir)/$(firedragon_basename)/firedragon install
	echo $${FIREDRAGON_VERSION#v} > $(firedragon_basename)/browser/config/version_display.txt

package: build
	@echo $(firedragon_basename).source.tar.zst >> clean.targets.txt
	tar $(COMPRESS_ARGS) -cf $(firedragon_basename).source.tar.zst $(firedragon_basename)

.PHONY: all copy build package
