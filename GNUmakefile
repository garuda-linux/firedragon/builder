export FLOORP_VERSION = 11.24.0

export FIREDRAGON_RELEASE = 2
export FIREDRAGON_VERSION = $(FLOORP_VERSION)-$(FIREDRAGON_RELEASE)

# Supported by Floorp:      ar cs da de el en-US en-GB es-ES fr hu id it ja ko lt nl nn-NO pl pt-BR pt-PT ru sv-SE th tr uk vi zh-CN zh-TW
export FIREDRAGON_LOCALES = ar cs da de el en-US en-GB es-ES fr hu id it ja ko lt nl nn-NO pl pt-BR pt-PT ru sv-SE th tr uk vi zh-CN zh-TW

export BUILD_DIR = build

export srcdir := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

export real_build_dir := $(abspath $(BUILD_DIR))

export floorp_basename := floorp-v$(FLOORP_VERSION)
export firedragon_basename := firedragon-v$(FIREDRAGON_VERSION)
export firedragon_catppuccin_basename := firedragon-catppuccin-v$(FIREDRAGON_VERSION)

export COMPRESS_ARGS := --zstd
export UNCOMPRESS_ARGS :=

all:
	@echo 'FireDragon Build System'
	@echo '======================='
	@echo ''
	@echo 'Commands:'
	@echo '    clean-full'
	@echo '        Delete BUILD_DIR'
	@echo '    clean-downloads'
	@echo '        Delete dowloads'
	@echo '    clean-folders'
	@echo '        Delete folders'
	@echo '    clean-targets'
	@echo '        Delete targets'
	@echo '    source'
	@echo '        Build source package'
	@echo '    source-catppuccin'
	@echo '        Build source package with catpuccin branding & theme'
	@echo '    linux-x86_64'
	@echo '        Build linux x86_64 package'
	@echo '    linux-x86_64-catppuccin'
	@echo '        Build linux x86_64 package with catpuccin branding & theme'
	@echo '    appimage-x86_64'
	@echo '        Build x86_64 AppImage'
	@echo '    appimage-x86_64-catpuccin'
	@echo '        Build x86_64 AppImage with catpuccin branding & theme'
	@echo ''
	@echo 'Variables:'
	@echo '    FLOORP_VERSION = $(FLOORP_VERSION)'
	@echo '        Floorp version to use as base'
	@echo '    FLOORP_CHECKSUM = $(FLOORP_CHECKSUM)'
	@echo '        Floorp source checksum'
	@echo '    FIREDRAGON_RELEASE = $(FIREDRAGON_RELEASE)'
	@echo '        FireDragon release number'
	@echo '    FIREDRAGON_VERSION = $(FIREDRAGON_VERSION)'
	@echo '        FireDragon version'
	@echo '    FIREDRAGON_LOCALES = $(FIREDRAGON_LOCALES)'
	@echo '        FireDragon locales'
	@echo '    BUILD_DIR = $(BUILD_DIR)'
	@echo '        Build directory'

$(real_build_dir):
	mkdir -p $(real_build_dir)

clean-full: $(real_build_dir)
	rm -rf $(real_build_dir)

clean-downloads: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/clean.mk clean-downloads

clean-folders: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/clean.mk clean-folders

clean-targets: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/clean.mk clean-targets

source: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/source.mk

source-catppuccin: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/source-catppuccin.mk

linux-x86_64: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/linux-x86_64.mk

linux-x86_64-catppuccin: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/linux-x86_64-catppuccin.mk

appimage-x86_64: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/appimage-x86_64.mk

appimage-x86_64-catppuccin: $(real_build_dir)
	@$(MAKE) -C $(real_build_dir) -f $(srcdir)/assets/appimage-x86_64-catppuccin.mk

.PHONY: all clean-full clean-folders clean-files source linux-x86_64
