# FireDragon ChangeLog

## [FireDragon v11.24.0-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.24.0-2) — 2025-03-07 — [`v11.24.0-1`…`v11.24.0-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.24.0-1...v11.24.0-2)

### 🚀 Features

- [**breaking**] Use zstd compression for appimage — [`7cb5e19b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7cb5e19b4f6a2baef9e23802dddc0e275fa8ded1)

### 🐛 Bug Fixes

- Set MOZ_APP_LAUNCHER in appimage to fix setting itself as default — [`117cbf46…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/117cbf468f62dd63f889afd7ea07c8820f6d1e64)

### ⚙️ Miscellaneous Tasks

- Simplify appimage jobs since they now download their own appimagetool — [`ed176827…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ed176827ba9e2ef97c33aacefe10681d2a33b407)

## [FireDragon v11.24.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.24.0-1) — 2025-03-05 — [`v11.23.1-1`…`v11.24.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.23.1-1...v11.24.0-1)

### 🐛 Bug Fixes

- Remove allow-ubo-private-mode.patch since Floorp already implements it — [`f49035b6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/f49035b6c20a7e1b8f37bbba97e02e50359a15f2)

## [FireDragon v11.23.1-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.23.1-1) — 2025-02-18 — [`v11.23.0-4`…`v11.23.1-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.23.0-4...v11.23.1-1)

### 🚀 Features

- Show cookie blocker settings in UI — [`0fde62a8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0fde62a860cf19c1a8a586e9d6cdff21b71281e5)
- Add script to simplify pulling & pushing the settings subtree — [`0ec778c5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0ec778c5cec9fdf2179f7df2ab69b145c070cf77)
- Show cookie blocker settings in UI — [`43c4bb90…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/43c4bb9042ec35a704b3ee756ed9ebf94d4509dd)

### ⚙️ Miscellaneous Tasks

- Do not force history preferences since they are default anyway — [`1a5385f7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1a5385f7ce8f66a13575fd964e877f778ee1fff7)
- Do not force history preferences since they are default anyway — [`e861aa05…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e861aa0527f4b5eea57ce5dddba64ed1a14b8927)
- Update settings subtree — [`3fa651b5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3fa651b5252f6253484c2e33fe843e559de51786)

## [FireDragon v11.23.0-4](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.23.0-4) — 2025-02-09 — [`v11.23.0-3`…`v11.23.0-4`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.23.0-3...v11.23.0-4)

### 🐛 Bug Fixes

- Readd new strict mode rules with migration to default category — [`b000f45f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b000f45f21cbc5a7f1470681e0990242e76b2779)
- Readd new strict mode rules with migration to default category — [`78c8ceff…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/78c8ceff3f2050b39639872b0678022a9c4d213e)

### ⚙️ Miscellaneous Tasks

- Update settings subtree — [`82bbf9ad…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/82bbf9ad6870d3d204cd72d0db89d14734f26369)

## [FireDragon v11.23.0-3](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.23.0-3) — 2025-02-09 — [`v11.23.0-2`…`v11.23.0-3`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.23.0-2...v11.23.0-3)

### 🐛 Bug Fixes

- Do not reset strict mode rules to keep compatibility. — [`46e61f0b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/46e61f0b39199b3afeef77086cc2dff895d0fa3c)

### ⚙️ Miscellaneous Tasks

- Update settings subtree — [`c65e3346…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c65e33468a3250295f9ec53e2f593672cfcae844)

## [FireDragon v11.23.0-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.23.0-2) — 2025-02-09 — [`v11.23.0-1`…`v11.23.0-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.23.0-1...v11.23.0-2)

### 🐛 Bug Fixes

- Remove privacy-preferences.patch and allow customization of content blocking — [`295c28fd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/295c28fdbe221b0b7f80f10209fdc96a3b45ca38)
- Remove already added prefs in pref-pane/firedragon.js — [`65a2e4a2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/65a2e4a26c759a5e8d07fbb956976c09a748b0ef)
- Remove privacy-preferences.patch and allow customization of content blocking — [`8acdf461…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8acdf461b166be51fc8a5cb221f890f50bc82860)

### ⚙️ Miscellaneous Tasks

- Update settings subtree — [`c4c0eafe…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c4c0eafe59bdb478900f83d6abea465836a5fd72)

## [FireDragon v11.23.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.23.0-1) — 2025-02-05 — [`v11.22.0-2`…`v11.23.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.22.0-2...v11.23.0-1)

### 🐛 Bug Fixes

- Made XDG File Picker setting editable and added descriptions — [`81282f30…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/81282f304ddcbbefeb9cfe04abacf86d628792f6)
- Updated cfg.version — [`c2f4f39e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c2f4f39edc7a6dd049b4992d1f23e7ec43a3aae6)

### ⚙️ Miscellaneous Tasks

- Update settings — [`9f76988c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/9f76988c5c5522ada35e11ef8cc4333bc1745ae7)

## [FireDragon v11.22.0-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.22.0-2) — 2025-01-18 — [`v11.22.0-1`…`v11.22.0-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.22.0-1...v11.22.0-2)

### 🐛 Bug Fixes

- Fix catppuccin icon resolutions — [`4e256358…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4e2563582b403145faf17107991dd2700bc6ceb4)
- *(firedragon-builder)* Replace firefox-wordmark.svg for new palette — [`75d7f20a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/75d7f20a9d62d0671b56e604029e69d8f25d9f80)

### ⚙️ Miscellaneous Tasks

- Remove obsolete variable to pull submodules — [`6a04c8a5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6a04c8a5414f0588b338a3910cd5e9948817dac0)
- Update catppuccin icon — [`efe0b4bd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/efe0b4bddfcbac8aecc536c6f77fae155ff8c3b1)

## [FireDragon v11.22.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.22.0-1) — 2025-01-08 — [`v11.21.0-1`…`v11.22.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.21.0-1...v11.22.0-1)

### 🚀 Features

- Initial addition of the catppuccin variant — [`cbfeb0cf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cbfeb0cf363e2a419a925cf488dd50c978b708ee)

### 🐛 Bug Fixes

- Fix build with python >= 3.12.8 — [`258391bf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/258391bf92e4c61f08b023ccbe50e0dd8d43245e)
- Tighten regex for replacement og theme in policies.json — [`72b3151e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/72b3151ebd9511f0d426b77c59157ca023c21ca3)
- Fix catppuccin branding wordmark — [`29651b3c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/29651b3c33e9d27c280a1ca2aafacf06bf64aa7f)

### 📚 Documentation

- Add readme section for branding and theme variants — [`43ab7d5f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/43ab7d5f20bf5ff5217ea3a5580c9dda7fbe8058)

### ⚙️ Miscellaneous Tasks

- Add ability to create piplines using GitLab UI — [`db8beed4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/db8beed4d55ddbeab4ea294af1394411546906d4)
- Fix "catppucin" to "catppuccin" — [`d547721b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d547721be5f6eb2df77ca6e3863f057066f5d056)
- Use correct target in appimage-x86_64-catppuccin — [`a23c4473…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a23c44730d5ca7127080de2823b795595a171c84)

### ◀️ Revert

- Fix: Fix build with python >= 3.12.8 — [`72783a14…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/72783a149c7c5cc75d41f63229bee0f5bf575435)

## [FireDragon v11.20.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.20.0-1) — 2024-10-31 — [`v11.19.1-2`…`v11.20.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.19.1-2...v11.20.0-1)

### 🚀 Features

- *(settings)* Re-added Librewolf uBO query stripping asset file — [`a7dd8386…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a7dd838657e800be0cba9b2c63296717cb5da2b3)

### ⚙️ Miscellaneous Tasks

- Update settings subtree — [`18e7d248…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/18e7d248dc111b8b009e3bcc7429cf5495044f9c)

## [FireDragon v11.19.1-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.19.1-2) — 2024-10-27 — [`v11.19.1-1`…`v11.19.1-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.19.1-1...v11.19.1-2)

### 🚀 Features

- *(firedragon-settings)* Documented Firefox Translate feature — [`bca1db76…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/bca1db769cb60c9d18873a4ca80208091105904e)
- *(settings)* Added most of Librewolf's 8.5 config values — [`b22a707a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b22a707a82d9e2a881e106ac93573fc37ba65c72)

### 🐛 Bug Fixes

- *(settings)* Fix premature closing of DOH section comment — [`66218550…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/66218550f82d0c5ee551b5bb7be1442baaa74e7f)
- *(settings)* Fixed format on a few commented out lines — [`1e605a19…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1e605a19a85f5d418800f8ffbcfe7008c81b905d)

### 📚 Documentation

- Add maintenance instructions for updating the settings subtree — [`bc3f6eb2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/bc3f6eb21080b7e60c0d14ecf0bfbefbee88fabb)
- Fix release script documentation since it no longer updates floorp checksum — [`278db8ca…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/278db8ca7162e8553a2768ee4b0f2e38ff4d4e0c)

### ⚙️ Miscellaneous Tasks

- Update settings subtree — [`751d38d9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/751d38d9927859b51bc3c1cc4735159b2718c58e)
- Update settings subtree — [`e7064768…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e706476836f7be0fc9b28b3a22e4b658c6703e54)

## [FireDragon v11.19.1-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.19.1-1) — 2024-10-10 — [`v11.19.0-1`…`v11.19.1-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.19.0-1...v11.19.1-1)

### 🚀 Features

- Use latest links for addons, update prefs for Floorp — [`63b410f7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/63b410f7ed34fbe7322558d5513477d133fb10dc)
- Added Fastfox tweaks to firedragon.cfg — [`e0af3b0a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e0af3b0a2789b4ae9b73a3f0cabc71c6f28c5e3d)
- Added Smoothfox tweaks + other fixes — [`dee13dad…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dee13dad2f990ced63297b22555097e7c150accb)
- Added FPP and some more firedragon.cfg tweaks — [`264c050c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/264c050c53b93bd250a9686277f45238a636fd54)
- Updated firedragon.cfg — [`0951dec1…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0951dec1deb47862e02eec9f2095f548cbf5ced9)
- *(firedragon)* Firedragon.cfg changes — [`fbe492b7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/fbe492b7f2af9039a840c0fc1bd0b3272efb47e2)
- *(firedragon)* Adding more settings in firedragon.cfg — [`ec75133c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ec75133cd06d3b872725fbc153e8eb85c70e403f)
- *(firedragon)* Bringing back firedragon.overrides.cfg feature — [`aa519f63…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/aa519f6338da3322689490b7717071cbcef8a568)
- *(firedragon)* Updated UserAgent and Sync settings — [`e7273fb8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e7273fb82186b6cde43e19ba26e6e9d2cda68eff)
- *(firedragon)* Add firefox-wordmark.png — [`ee318352…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ee3183524d55d3f1d91a523f2fd08e9fcb6b7ca2)
- *(firedragon)* Improving default User Agent — [`ef68ef79…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ef68ef7903a0089f6a15638f8c4cba0a98ad361f)
- Update firedragon.cfg from Librewolf — [`06e546ce…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/06e546ce1fc3d4619b10603efdc0ac1e3a47c91a)
- *(firedragon-settings)* Countless number of updates in firedragon.cfg — [`1aeba9b4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1aeba9b447dabb53bc601c4f659784a0dc6482a8)
- *(firedragon-settings)* Added Flagfox extension to policies.json — [`43c3b99f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/43c3b99f86456b79a426f8098041ead54e03b797)
- *(firedragon-settings)* Added Wayland fractional scale as default in firedragon.cfg — [`522b0239…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/522b0239a81e3aa1217795208f58db708332cc47)
- *(firedragon-settings)* Added First Run page in policies.json — [`fdb8dc9a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/fdb8dc9a83b2ee2660089b230111732655388bbd)

### 🐛 Bug Fixes

- Updated firedragon.cfg to 8.1 — [`b368bf90…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b368bf90af84240d596ec4f792814bf7d9279387)
- Updated firedragon.cfg removing fill urlbar — [`42740f3e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/42740f3e7a09c8d2d3f9b85fb44a6a910702e47e)
- *(firedragon)* Updated Sync settings back to Garuda server — [`df4b02b5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/df4b02b5a4e382a135edcb9ec7de9b06355c4fb3)
- *(firedragon)* Fixed how to handle recent Custom UserAgent when FP/FPP is disabled — [`06ff221e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/06ff221e8c8264db6fc08c067935df0f07e6aea9)
- *(firedragon)* Add firefox-wordmark png — [`507158f7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/507158f7c56f8c8d8ebab282f0d17003afcb74d8)
- *(firedragon)* Fixed Clipboard setting — [`84e38f2b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/84e38f2b297cfeaa4a42536c6031770296942da0)
- *(firedragon)* Adjustments to Fingerprinting settings — [`8b235a8a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8b235a8a102a78a023d670470807c53e41b32ae8)
- Default search engine not applying — [`a79d41dc…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a79d41dc0d7394d8a6e75966f3c455ca065e240f)
- *(firedragon)* Update README.md and bring back TopSites in New Tab — [`619c506c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/619c506c6ed22cd87fc224ad33c9565fa1edc648)
- *(firedragon)* Removed Fingerprinting as default — [`cf9cdf91…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cf9cdf91240abb1487b4b8260c68431577d3a5a1)
- *(firedragon)* Fixed login error on Garuda Forum — [`ac379f87…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ac379f87d53a5fc92f8559d2e334f634d253740c)
- *(firedragon)* Updated choices for User Agent in firedragon.cfg — [`11e0c235…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/11e0c235803ddb6cc7eaa5e69ce2151f77fb64e6)
- Do not force history prefs — [`863f9fd3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/863f9fd3ba358b8d8c87a106405e1d4daf0a87f3)
- *(firedragon-settings)* More updates on firedragon.cfg — [`a5e82a60…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a5e82a60a1579b8c6cdfdb187d71710ccc914987)
- *(firedragon-settings)* Fixed for spoofing Windows on UA — [`94fc0f04…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/94fc0f045273065f3b1b6c5e3b8421efa742e9ed)
- *(firedragon-settings)* More comment precision on line 224 in firedragon.cfg — [`dee51102…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dee5110229fed9809ad7d6a0ac9898bc72dd8f22)
- *(firedragon-settings)* Fix in firedragon.cfg for Floorp's very sad removal of their User Agent Section in 11.17.4 — [`04a7d54f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/04a7d54faaa5b5efb1ca7d4880b2545d9bc92488)
- *(firedragon-settings)* Removed duplicate in firedragon.cfg — [`66d19e4b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/66d19e4b443144b1dc820136a02396b5ab450282)
- *(firedragon-settings)* Removed override_url in firedragon.cfg — [`5db055ea…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/5db055eab90f0b13eb742cea8f8e6b399a59764f)
- *(firedragon-settings)* Removed conflicting word in firedragon.cfg — [`997b6372…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/997b637202daf8f683eaf8d2844a742fe7c5d366)
- *(scaling)* Disable key causing issues for a lot of people — [`d4337abd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d4337abd013bcf3128ea9cbeee161f2355a8ee36)
- Use firefox user agent — [`3f899f73…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3f899f7362d8fe5639750bdaf1f6d11e48a08029)
- Update about dialog screenshot — [`61705e6b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/61705e6b7489e4e86b238c4bd67ebe187f8b5a71)

### 🚜 Refactor

- Clone floorp source directly insead of relying on floorp-source — [`4f273a80…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4f273a805abc0fb5ed10c575904451cf6c3e5689)

### 📚 Documentation

- Update README/screenshots with the new base — [`2529a524…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/2529a524650da00733b7309a22c8be5fe1ca29a8)
- Update README — [`64e537ab…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/64e537ab04dfd0450fc9fb2d2e9837ecd4dad9c9)

### ⚙️ Miscellaneous Tasks

- Delete settings subtree — [`d5ccf8ac…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d5ccf8ac0801f6cd71dc84e56a70cde86385cbc0)
- Add building AppImage — [`a8f6d7d4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a8f6d7d4e3cd60380b0a7929529fe0c1f5863424)
- Fix yaml — [`87b1aa1a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/87b1aa1ac7f22eb7def1e799c03e1f6dba87cc2e)
- *(nix)* Use determinatesystems nix installer action to likely fix the appimage build issue — [`8a9ca4f4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8a9ca4f4d74ed815ffc683d31303738d5b652118)
- Fix ci trying to build firedragon rather than fetching it from chaotic-nyx — [`4a29d148…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4a29d148fe9fc14eec4930dc0cf41ab33699a675)
- *(firedragon-settings)* Add Makefile — [`14227844…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1422784463e2fecbcd1ca4c48afe669b99fae1a1)
- Upstream firedragon.desktop — [`4f1e4348…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4f1e4348e5ea07cd888fdb3ab54120dfca5ac3b2)
- Remove history and session restore section — [`6abced36…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6abced368d0df74064966861e497c1360872d300)
- Upstream metainfo — [`e424f8dc…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e424f8dc3b84a4c8946e3000bbb1539f116ba36c)
- Add release v11.15.0-1 to metainfo — [`42607907…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/426079070dab0eaa1d2ddcfbba4f33b085a64455)
- Add v11.16.0-1 to metainfo — [`fcd899e3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/fcd899e326e5e4af18589fd7270b8e6ac3a629f3)
- Update policies.json from Librewolf — [`a1468752…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a1468752c2d47b8b165d693783e0ba95354e543c)
- Remove obsolete submodule — [`064a051b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/064a051b23e2efcd12045539df83ed1df01eb3e6)
- Delete metainfo — [`d3537748…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d3537748090bfefbc2d5b0c303999bb9859d0255)
- Reinitialize settings subtree with entire history — [`63741da4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/63741da40ed7a0e405f894587c1707e065379ccd)

### README

- Add information about AppImage builds — [`4ba2077f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4ba2077fb9d1aee418535ab373f20e177a76cb60)

## [FireDragon v11.19.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.19.0-1) — 2024-10-01 — [`v11.18.1-2`…`v11.19.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.18.1-2...v11.19.0-1)

### ⚙️ Miscellaneous Tasks

- Remove settings submodule — [`9420dee7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/9420dee7a6c07e61882120da15fc324ca1b91e38)
- Initialize settings subtree — [`616b60e9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/616b60e9ee9d8a04de4bfdd77cc6b9fd64d2f5c8)
- Update changelog header — [`f792dc4e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/f792dc4e8ed63e46ca7685faf9b51ab3ae21e4b5)
- Update changelog template — [`a6c1cc26…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a6c1cc26671069f3ddcbf4c83c37f5056b1380da)

## [FireDragon v11.18.1-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.18.1-2) — 2024-09-22 — [`v11.18.1-1`…`v11.18.1-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.18.1-1...v11.18.1-2)

### 🚀 Features

- Add ja-JP-mac & vi translations to privacy-preferences.patch — [`79d4ff26…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/79d4ff2612d3b270a28436a716c071ccef58c088)
- Add ja-JP-mac & vi translations to pref-pane — [`d0228a2b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d0228a2bb6d2e5585e0207973c1ef3541ef6cb55)
- Add ja-JP-mac & vi to FIREDRAGON_LOCALES — [`21fa75f2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/21fa75f2d867beedc390f306a76f2557bafed0e6)
- Add about-contributors.patch — [`0161b8c9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0161b8c9e757e8d50bf723ffb647462b006cf056)
- Add about-dialog.patch — [`cee43cff…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cee43cffb6fc1189b3b7b17d3440816fd1739fea)
- Add translations to about-dialog.patch — [`d54c84e0…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d54c84e020020a04c2135b360f9a03a4d02ccc46)
- Update about dialog — [`601bed1f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/601bed1fcfebc33d7a9b32d3dac76e70343848a7)

### 🐛 Bug Fixes

- Fix package extension in source_basename for appimage — [`86941836…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/86941836f72d72bab76929ac5453c7f729f3971c)
- Remove ja-JP-mac translations — [`d77e2752…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d77e27528c3b5e50c271de26187eb98272505c1d)
- Fix list of contributors int about-contributors.patch — [`1c4e5dfb…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1c4e5dfb83b8243e3bcba65d8b875500d18cf7e3)
- Update FireDragon about message — [`87ab63cc…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/87ab63cc349e6d149124a1d3a713029dd66b47e4)
- Remove ja-JP-mac translations from about-dialog.patch — [`ce1e8fb4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ce1e8fb4c27be4d6d3df4fa1f95f540fe202cd1a)
- Add workaround to support tar without native zstd support — [`0104b111…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0104b111477ad3dd992203de0292167d50e84dce)

### 🚜 Refactor

- Replace lock-is_esr-flag with allow-searchengines-non-esr patch — [`7ee41e54…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7ee41e544d843e0b9b6dc9a0a7425881ae7d3481)
- Use zstd compression for linux package as well — [`3744e3c0…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3744e3c01d4711f4efcbf302e7ddb702b42d2287)

### 📚 Documentation

- Add information about release.sh script to README.md — [`e5976fa2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e5976fa2db06dc51b4c30bc985dbfb2b48d436c7)

### ⚙️ Miscellaneous Tasks

- Add git-cliff for changelog generation — [`acceb34a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/acceb34a3f0f575034d2497f22cd1e3f558fa639)
- Add version changelog to release description — [`0c3d7e69…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0c3d7e696b9d3c627d38fcc6ea0ab7e804af40d6)
- Add release.sh script for easyily creating releases — [`58f9c43c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/58f9c43c8b582c63f31af1cfd52af6f0fde3503c)
- Fix CI config — [`10c19303…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/10c193036ffc2859227bc23008b79ee063a0d3e1)
- Support updating the Floorp checksum using the release.sh script — [`a4f81805…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a4f81805e027a21329bc22c01ecf81bda50a8207)
- Fix spacings in changelog — [`21cc885b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/21cc885bb1cac52b46629bf7d8d61a8e5f1f916d)
- Fix changelog job — [`8aca14ff…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8aca14ff115a7deae8bd4d15cf6e7d09bcd339d7)

## [FireDragon v11.18.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.18.0-1) — 2024-09-03 — [`v11.17.8-1`…`v11.18.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.8-1...v11.18.0-1)

### 🚜 Refactor

- Make linux build of source archive — [`a12791b7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a12791b72a4654f7b1ff49d6b53485f7d4c319c6)

### ⚙️ Miscellaneous Tasks

- Clean up directory between appimage builds — [`49718375…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/49718375d03a6f3cdc974ff9204e9113d4c983a9)

## [FireDragon v11.17.7-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.7-2) — 2024-08-26 — [`v11.17.7-1`…`v11.17.7-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.7-1...v11.17.7-2)

### 🐛 Bug Fixes

- Remove organization policy banner — [`48344a95…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/48344a95288386f45ac767e3f084f09492fbf8c5)
- Do not override browser/config/version.txt — [`dca023af…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dca023af6eaf51bdac2e5de1f2b4693afee0885d)
- Fix filename for built package — [`25829654…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/25829654483b66d429437ae2788ea8634ed362e3)

### ⚙️ Miscellaneous Tasks

- Update settings submodule — [`506971a5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/506971a5b3d682b6aa522b583a98ed0a80206dc6)

## [FireDragon v11.17.6-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.6-1) — 2024-08-22 — [`v11.17.5-1`…`v11.17.6-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.5-1...v11.17.6-1)

### ⚙️ Miscellaneous Tasks

- Update to v11.17.6 — [`4556f94f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4556f94f7143a00492d26e8e16a51b86f8bb5204)
- Delete 0036-bmo-1912663-cbindgen-0.27.0-fixes.patch — [`36499786…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/364997865f1ee199f34fda3e86f3cc8928026979)
- Update settings submodule — [`0e33800d…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0e33800d18212cedd798bf9cc933aa4be9a29861)

## [FireDragon v11.17.4-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.4-2) — 2024-08-19 — [`v11.17.4-1`…`v11.17.4-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.4-1...v11.17.4-2)

### 🐛 Bug Fixes

- Fix exec path in appimage desktop file — [`f80e8603…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/f80e860389dd6b7f03af41ced28da7a54636c84a)

### ⚙️ Miscellaneous Tasks

- Fix appimage filename in release — [`2be56f4d…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/2be56f4d10af1739bab592fb13a34dd455d78f4e)
- Update settings submodule — [`8f765ea8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8f765ea8f05d97c0edfb7b29d303eb5adb119b86)

## [FireDragon v11.17.4-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.4-1) — 2024-08-18 — [`v11.17.3-2`…`v11.17.4-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.3-2...v11.17.4-1)

### 🐛 Bug Fixes

- Remove obsolete LD_LIBRARY_PATH override — [`ee9fd5e6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ee9fd5e65a5cf4a50080c752d5cbc1d4c65e2d35)

### 🚜 Refactor

- Integrate app image generation into existing make infrastructure — [`7443bae1…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7443bae13c0ba090f4395f6b72db5197db8e04c6)

### ⚙️ Miscellaneous Tasks

- Use settings submodule for desktop file instead of download — [`4051f7af…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4051f7aff89804da36254ff5bd230ff3ededff85)
- Install curl for appimage job — [`2aad3e83…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/2aad3e836f19c9624d391b670b7702c0a1c0d9cd)
- Fix appimage upload file — [`254e7c6c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/254e7c6c036f22440d2e22ec8875470f076af841)

## [FireDragon v11.17.3-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.3-2) — 2024-08-18 — [`v11.17.3-1`…`v11.17.3-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.3-1...v11.17.3-2)

### 🚀 Features

- Initial AppImage bringup (proper AppImage, not hacked NixOS one) — [`03a26a0e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/03a26a0eea2719b2afa8af052dcf693cbcdad3fe)

### 🐛 Bug Fixes

- *(appimage)* Actually keep artifact; submodule update — [`e46305d7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e46305d7418d2e33f55835ceca53332c8b4ef4e2)
- Unterminated quoted string — [`e3fb87be…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e3fb87be7af2e92ab82ae640188d51891b1058c5)

### ⚙️ Miscellaneous Tasks

- Update settings submodule — [`1a2f1586…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1a2f15868135e895aa6b50acba17e01581fa9107)

## [FireDragon v11.17.3-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.3-1) — 2024-08-17 — [`v11.17.1-1`…`v11.17.3-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.17.1-1...v11.17.3-1)

### 🐛 Bug Fixes

- Add lock-is_esr.patch to lock ESR detection — [`8457f05d…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/8457f05da128a204e017e4274a49334dc5ed13b6)

## [FireDragon v11.17.1-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.17.1-1) — 2024-08-17 — [`v11.16.0-1`…`v11.17.1-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.16.0-1...v11.17.1-1)

### 🐛 Bug Fixes

- Fix pref-pane-small.patch for v11.17.1 — [`90d0b8dd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/90d0b8dd2f64fb4acea799a5477faf70c0b8444e)
- Fix privacy-preferences.patch for v11.17.1 — [`dc56e83f…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dc56e83f3b12a81504496494beb5b1f55e55475e)
- Fix settings.patch for v11.17.1 — [`f9f34cc6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/f9f34cc63726c9286b8b663a41f2863cdde95a2f)
- Fix MOZ_SERVICES_HEALTHREPORT in mozconfig — [`9cef1209…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/9cef12095cefb662cb55d6611afbfb1228186b57)
- Include fix for cbindgen 0.27.0 — [`ab3a53a3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ab3a53a364f5fba707fa7890de3687b4ef60ac02)

### ⚙️ Miscellaneous Tasks

- Update patches to LibreWolf 128.0.3 ones; bump version to 11.17.1 — [`6c2053f9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6c2053f9780fc89c84a17bf1ac5a91f7a36a578a)
- Remove llvm & rust workarounds — [`567c1600…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/567c1600326516c896eab00d18397ab06ab77f6e)
- Update checksum for v11.17.1 — [`7dcce94a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7dcce94a0c426d58447ed10972799c07bc8d145d)
- Update settings submodule — [`37d0ddec…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/37d0ddecdeff8bc0070725d6041e8097252e62bf)
- Use rust instead of rustup — [`ab34b1b8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ab34b1b8538ccd5793f945f8b52369f7485a0c3a)

## [FireDragon v11.15.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.15.0-1) — 2024-07-15 — [`v11.14.1-1`…`v11.15.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.14.1-1...v11.15.0-1)

### 🐛 Bug Fixes

- Remove private components flag — [`5e4ec054…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/5e4ec05426b55ca0ac645b70869fb60b0111b995)
- Use LLVM 17 to build — [`223c1b76…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/223c1b76d7cdf82ead91c720ac0804a5d6fd58d9)
- Use wasi-compiler-rt17 — [`53edd188…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/53edd1881258cfab146014176bd176ec1cbfa2f8)

### ⚙️ Miscellaneous Tasks

- Update to floorp v11.15.0 — [`906b88c2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/906b88c2e22a57df21f00c7f08e02d844736fb2b)
- Update floorp source url — [`e329ffe6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e329ffe66cad8fb452a414aef846b253e52d69d0)
- Update floorp source url and checksum — [`194490c9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/194490c9adc979dfefb198b61e61d5e9ab944723)
- Add release job — [`bff1fdfe…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/bff1fdfe7a0e96d0d328c7833d76f1e63d1e5c8e)
- Use paru — [`03d0d986…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/03d0d986894b06aec598acf91aa04ef9abdfb18d)
- Fix installation of curl for publish — [`272e11db…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/272e11db18409775f263a14fc932cef090b8745d)

## [FireDragon v11.14.1-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.14.1-1) — 2024-06-26 — [`v11.14.0-2`…`v11.14.1-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.14.0-2...v11.14.1-1)

### ⚙️ Miscellaneous Tasks

- Update settings submodule — [`a380a6e4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a380a6e4373922f8fa192ba7c12b341087a11ba8)

## [FireDragon v11.14.0-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.14.0-2) — 2024-06-12 — [`v11.14.0-1`…`v11.14.0-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.14.0-1...v11.14.0-2)

### ⚙️ Miscellaneous Tasks

- Update settings — [`31164faf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/31164fafd59d7a02c43b6a7e4ba6313ba416d038)
- Release v11.14.0-2 — [`bdc16431…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/bdc16431360f5b1ef504e46ebab58abab055bde3)

## [FireDragon v11.14.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.14.0-1) — 2024-06-11 — [`v11.13.3-1`…`v11.14.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.13.3-1...v11.14.0-1)

### ⚙️ Miscellaneous Tasks

- Update floorp source to v11.14.0 — [`2b9c7b32…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/2b9c7b32b96b9b022a009f29df97c5537a96b183)

## [FireDragon v11.13.3-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.13.3-1) — 2024-05-25 — [`v11.13.2-1`…`v11.13.3-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.13.2-1...v11.13.3-1)

### ⚙️ Miscellaneous Tasks

- Release v11.13.3-1 — [`d07e99c8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d07e99c866eeb0584a179de96dfbb31fdbb9778b)

## [FireDragon v11.13.2-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.13.2-1) — 2024-05-19 — [`v11.13.1-1`…`v11.13.2-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.13.1-1...v11.13.2-1)

### ⚙️ Miscellaneous Tasks

- Release v11.13.2-1 — [`6b0415f7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6b0415f715cbd4435baf428a517f593917ba5325)

## [FireDragon v11.13.1-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.13.1-1) — 2024-05-18 — [`v11.13.0-1`…`v11.13.1-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.13.0-1...v11.13.1-1)

### ⚙️ Miscellaneous Tasks

- Release v11.13.1-1 — [`18f0e58a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/18f0e58a469ece8ee140d6b7a6770bded13973ac)

## [FireDragon v11.13.0-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.13.0-1) — 2024-05-17 — [`v11.12.2-2`…`v11.13.0-1`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.12.2-2...v11.13.0-1)

### 🐛 Bug Fixes

- *(firedragon)* Fix compression for source archive — [`b8132544…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b81325447c91751a4bc1190629dd109281a2c58e)
- *(firedragon)* Fix pref-pane-small.patch for floorp v11.13.0 — [`c25839d5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c25839d5b6c9bef9655afa869e50d7e49689bbcc)

### ⚙️ Miscellaneous Tasks

- *(firedragon)* Release v11.13.0-1 — [`e6f3fedf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e6f3fedfc845ebea715bc5111f840326bd063bd2)

## [FireDragon v11.12.2-2](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.12.2-2) — 2024-05-13 — [`v11.12.2-1`…`v11.12.2-2`](https://gitlab.com/garuda-linux/firedragon/builder/-/compare/v11.12.2-1...v11.12.2-2)

### 🐛 Bug Fixes

- *(firedragon)* Delete pingsender from built archive — [`7af67773…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7af677738411998c5545dfa5e6c1ed5b72a7be2e)

### ⚙️ Miscellaneous Tasks

- *(firedragon)* Release v11.12.2-2 — [`43826119…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/43826119d9a23b5bb17c4cf981de8d93264caac0)

## [FireDragon v11.12.2-1](https://gitlab.com/garuda-linux/firedragon/builder/-/releases/v11.12.2-1) — 2024-05-12

### 🚀 Features

- *(firedragon)* Testing UI patch — [`e17b6e23…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e17b6e23f5f6ceccb652a34c8d64b86147e2be24)
- *(firedragon)* Testing UI patch test2 — [`37224059…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3722405952fab6b9e879a1b1f30682e5827090e1)
- *(firedragon)* Testing UI patch test3 — [`62966664…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6296666428890ce156e2982c2d6032a7d2252924)
- *(firedragon)* Removing test UI patch — [`ca792ebf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ca792ebf5b879948f5eb14c670c7d85c60a3fc16)
- *(firedragon)* Patched preferences-floorp.css — [`26ae78c9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/26ae78c9d0408edd8a15be3f910d3c734a663920)
- *(firedragon)* Replacing firefox-wordmark — [`3514c7c2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3514c7c20feaea6b17afe4821b25a5fd5b31ce06)
- *(firedragon)* Test #2 replacing firefox-wordmark — [`2841a4fc…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/2841a4fc25845252bf74bdceeb2e503ad261ae9a)
- *(firedragon)* Test #3 replacing firefox-wordmark — [`291cbfcf…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/291cbfcfe899d493a23c77a8e60522a756c36620)
- *(firedragon)* Cleaning old file — [`1f1caac4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1f1caac46cb187d7e7e4745c6896408608b61deb)
- *(firedragon)* Update floorp source to v11.11.0 — [`dad96b28…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dad96b28c01cfad6ec6dfe8fa4be47bd5ddf7e5b)
- *(firedragon)* Use archlinux:base-devel for CI — [`f128f1b1…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/f128f1b1214a9137e315a7b775ccfeb0e76b8b37)
- *(firedragon)* Include private components — [`ff652de3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ff652de321b1318095419690069c77e97add4a6d)
- *(firedragon)* Hide ETP category setting — [`925cf6d6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/925cf6d6f4b8342a326ff0428f77ff0f86709f43)
- *(firedragon)* Add build step to include other locales — [`72afc4d9…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/72afc4d969b8f7224aa3335a80cf6d34277b212f)
- *(firedragon)* Add translations to privacy-preferences.patch — [`a02d679a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/a02d679ac72d18248f59ef6ab995b98ac9c0986c)
- *(firedragon)* Add german pref-pane ftl — [`ad78258a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ad78258a69de07193d152dd57e61ba8667e74e37)
- *(firedragon)* Add Thai, traditional and simplified Chinese locales — [`ecf87cf3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ecf87cf3125e25607fbee430e88a7ee1e1ecb844)
- *(firedragon)* Add Thai and Chinese support to privacy-preferences.patch — [`43b9780b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/43b9780b145cec19de8eb702722e61c5386fc210)
- *(firedragon)* Add arabic pref-pane ftl — [`28b08ff6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/28b08ff63d7c9f51e2fc524e1b88b8a1f960cd54)
- *(firedragon)* Add czech pref-pane ftl — [`852cf890…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/852cf890300c9f94b2066cce80c97fdbbe0203f2)
- *(firedragon)* Add danish pref-pane ftl — [`47ae0b47…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/47ae0b47649ebfcbaaf1ea78752d0fdc1a3aa7dc)
- *(firedragon)* Add greek pref-pane ftl — [`ab176132…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ab176132659301cbc5d2ea77160d7a8af0d6f0cf)
- *(firedragon)* Add british english pref-pane ftl — [`d9bf38f3…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d9bf38f318edc4788550e62aed96ab701660ff06)
- *(firedragon)* Add spanish pref-pane ftl — [`3eae1660…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3eae1660afbce6b7442f5d744e98badfd6a0b9c5)
- *(firedragon)* Add french pref-pane ftl — [`e5034ba2…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e5034ba280016c38c47e2d9c816a5ab1e85a5ffe)
- *(firedragon)* Add hungarian pref-pane ftl — [`c83993c0…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c83993c01cc0757393956661c5ba2f43d696353a)
- *(firedragon)* Add indonesian pref-pan ftl — [`35fbf36c…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/35fbf36c64720cfcdd2237f65a421e0473eb4b7d)
- *(firedragon)* Add italian pref-pane ftl — [`33cb7fdb…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/33cb7fdb59e69a20c53abae8d798224d91dab62e)
- *(firedragon)* Add japanese pref-pane ftl — [`13b5787e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/13b5787e549fc68fe50fa8aa36f96237c3c81091)
- *(firedragon)* Add korean pref-pane ftl — [`12840568…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/128405687dcb9b7d2a67886d0450186b07e48806)
- *(firedragon)* Add lithuanian pref-pane ftl — [`3d00c741…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3d00c7413cedae65b3721cf471609384c878f17d)
- *(firedragon)* Add dutch pref-pane ftl — [`66510086…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/66510086ab37d8d10a021f6ab380ff96f7279abf)
- *(firedragon)* Add norwegian pref-pane ftl — [`eaf07f78…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/eaf07f78ee4f9d69723e71673374bdc6e4ea5a78)
- *(firedragon)* Add polish pref-pane ftl — [`898cd542…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/898cd5429a2f8c8502426cf736e837b969351e17)
- *(firedragon)* Add brazilian portuguese pref-pane ftl — [`c357e702…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/c357e7029268bc22047e49d69425eb41b3c2be2b)
- *(firedragon)* Add european portuguese pref-pane ftl — [`4be941af…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4be941afcc1ecf6c697bea8bb8e9799973320015)
- *(firedragon)* Add russian pref-pane ftl — [`cefc99bd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cefc99bdfdd09570b6fc91d195f42be5ef2d2a30)
- *(firedragon)* Add swedish pref-pane ftl — [`7c7a9458…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7c7a9458d43e03053723f9a57487c02e0d12a52f)
- *(firedragon)* Add thai pref-pane ftl — [`acb7de19…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/acb7de19ab16c45cff4f676879472752c16ed7b6)
- *(firedragon)* Add turkish pref-pane ftl — [`ec6d0c0a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ec6d0c0a43a575226cdba3e65fe30a0f67fe7fc4)
- *(firedragon)* Add ukranian pref-pane ftl — [`55bfb104…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/55bfb104c08a0ce6f831fc0b81e85b1441af9b81)
- *(firedragon)* Add simplified chinese pref-pane ftl — [`56e47326…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/56e4732648313be259790cd831bd26890dee734c)
- *(firedragon)* Add traditional chinese pref-pane ftl — [`7c786b10…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7c786b1042ab1d6d0bf123d28438dd467251146d)

### 🐛 Bug Fixes

- Updated Firedragon logos — [`7716eb54…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7716eb5420819331211dc1b408191a14b8ce6749)
- *(firedragon)* Improved Wordmark SVG — [`6f4b1b06…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6f4b1b0689f120b85c0123a86593db83cdddb3f6)
- *(firedragon)* Only run CI builds on tags — [`d6faff74…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d6faff74cb13a054ab6dd4dfb87244ea701706dd)
- *(firedragon)* Make CI work without token — [`390639ed…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/390639ede3bddbda681011a6f26ea62d253dad67)
- *(firedragon)* Add missing targets to .PHONY in GNUmakefile — [`6ea364c4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/6ea364c4782ae5fd54d067f004aedf41d87423ee)
- *(firedragon)* Fix typos for clean-private-components target — [`cfa0b988…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cfa0b988bf13515a63543f4ddf51ba0fc939bf9d)
- *(firedragon)* Add build dir as dependency for clean targets — [`ffd196c4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/ffd196c402ee718f3ae5c33056224e3d7bd2f930)
- *(firedragon)* Do not store token in local repo config — [`abc04655…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/abc04655b1230fa1a7170bfda4869c6bcaaf2de5)
- *(firedragon)* Update pref pane category image — [`7da37683…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7da3768324bc143c2b66f4fc900befc502826bc0)

### ⚙️ Miscellaneous Tasks

- *(firedragon)* Update floorp source to v11.11.1 — [`e7afac25…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e7afac25b9e1efff3733c728ba11fda74f16509f)
- *(firedragon)* Update settings submodule — [`4687def6…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4687def68c82704e316d781513c9135013585915)
- *(firedragon)* Update settings submodule — [`34264dcd…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/34264dcd2e8e5768fbe474b8929220694c701a82)
- *(firedragon)* Fix FLOORP_PRIVATE_TARGET in help — [`d385b5a1…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/d385b5a111a49da6e12f5755ef57752230ccc51f)
- *(firedragon)* Use .git suffix for private repo — [`10b4c0ba…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/10b4c0ba834507df6bf0698537286837cc690380)
- *(firedragon)* Add comment for private components — [`7f70f9e1…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7f70f9e144a0f677ca1bd22806c81aa216e108d1)
- *(firedragon)* Remove private components workaround — [`b238fa02…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b238fa02ace521aeb4102e396004cd2daa9f2ff1)
- *(firedragon)* Update to v11.12.0 — [`33653985…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/336539852e4e01a2ea946b0f7c80b6002fb78166)
- *(firedragon)* Update floorp source to v11.12.2 — [`3b44c26a…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3b44c26adaea9c2db93cdb622f1df643a7f54f6a)
- *(firedragon)* Update versioning system — [`0b3f81de…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/0b3f81deeb728223c7b514aa2788265506d5b008)
- *(firedragon)* Remove checksum stage and artifact — [`b2aa18b4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/b2aa18b449ffc71191f48f0bbf53e6476216e3cc)
- *(firedragon)* Fix linux-x86_64 file extenion in artifact upload — [`1a8b0ef7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/1a8b0ef777fc7502058651183db7bcd945fb2c7b)
- *(firedragon)* Remove obselete private-components target from help message — [`e8b78ee4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/e8b78ee450c78558a57d8626539d1b685aea4e56)
- *(firedragon)* Clean obselete phony targets from GNUmakefile — [`4e3dfd87…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4e3dfd87115b1c414c1da9431370b74bd607f4e9)
- *(firedragon)* Refactor patches makefiles — [`4201c2c8…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/4201c2c80c8db70b6d5d52352bbd2ac82afe8c01)
- *(firedragon)* Cleanup unused patches — [`5dea3c2d…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/5dea3c2dc430ca591f3deab4932e30747e14e535)
- *(firedragon)* Decrease patches folder depth — [`171f3fa4…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/171f3fa47ec726ab1f7f541f125ce5559c72d4b6)
- *(firedragon)* Move locales to shared variable — [`3deaf60e…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/3deaf60ec7384b3e6bb4d5ad01a792704189590a)
- *(firedragon)* Use placeholder in pref-pane ftl — [`320ad070…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/320ad0700634e1f99a3c901960124432116f8121)
- *(firedragon)* Refactor pref-pane makefile — [`fb08072b…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/fb08072be3d904c48019d05d596f5e795c2022a0)
- *(firedragon)* Add prereq to pref-pane locales static pattern target — [`55100c3d…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/55100c3df41186c3a95ab35fcb061eea844783bb)
- *(firedragon)* Downgrade rust to 1.77 to fix issues around packed_simd — [`7eeefe17…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/7eeefe1747021517c8b8f1efbed05eb4778a8efe)
- *(firedragon)* Add README.md with build instructions — [`fd7bbce7…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/fd7bbce74e797e28d9f483c511568965b4d9b95e)
- *(firedragon)* Switch to official settings repository — [`cd713eb5…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/cd713eb5300a30cab684760cfa1e85f04b28d920)
- *(firedragon)* Remove v-prefix from version when uploading to package registry — [`dc0aeb12…`](https://gitlab.com/garuda-linux/firedragon/builder/-/commit/dc0aeb121a46389723fad74eed1b90020f1143a3)

<!-- generated by git-cliff -->
