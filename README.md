# FireDragon Builder

This repository contains the patches and code requied to build the FireDragon browser.

## Build commands

### Source archive

``` sh
make source
```

This downloads the Floorp source (if required), applies all patches, copies additional source files and installs FireDragon settings. Output source archive: `build/firedragon-vX.X.X-X.source.tar.zst`.

### Linux x86_64 archive

``` sh
make linux-x86_64
```

This creates the source files for FireDragon (if required) and builds it for linux x86_64 architecture. Output linux x86_64 archive: `build/firedragon-vX.X.X-X.linux-x86_64.tar.zst`.

### AppImage x86_64

``` sh
make appimage-x86_64
```

This uses the Linux x86_64 archive (building it if required) to create the FireDragon AppImage for x86_64 architecture. Output AppImage x86_64: `build/firedragon-vX.X.X-X.appimage-x86_64.AppImage`.

## Branding & theme variants

FireDragon provides branding & theme variants:

- `catppuccin`: Used by Garuda Mokka

To build such a variant just append `-xyz` to the make target. For example: `make source-catppuccin`

## Additional scripts

The following scripts are located in the `scripts` directory.

### `release.sh`

Used by maintainers to generate a new release. Automatically update Floorp version & FireDragon release number, update ChangeLog and tag the new release.

### `settings.sh`

Used by maintainers to pull & push updates to [FireDragon settings](https://gitlab.com/garuda-linux/firedragon/settings).
